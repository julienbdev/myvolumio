package com.julienbdev.myvolumio

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log

class NotificationReturnSlot: BroadcastReceiver() {

    override fun onReceive(context: Context?, intent: Intent?) {
        Log.d("myvolumio", "NotificationReturnSlot onReceive")
        if (intent != null) {
            MainActivity.channel.invokeMethod(intent.action, null, null)
        }
    }
}