package com.julienbdev.myvolumio

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.media.MediaMetadata
import android.os.Build
import android.support.v4.media.MediaMetadataCompat
import android.support.v4.media.session.MediaSessionCompat
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import java.io.IOException
import java.net.URL

class MediaNotificationManager(private val context: Context) {
    private val channelId = "MEDIA_NOTIFICATION_CHANNEL"
    private val mediaSessionTag = "MEDIA_SESSION_TAG"
    private val notificationId = 1
    private val builder: NotificationCompat.Builder
    private val mediaSession = MediaSessionCompat(context, mediaSessionTag)
    private val mediaMetadata = MediaMetadata.Builder().putLong(MediaMetadata.METADATA_KEY_DURATION, -1L).build()

    private var smallIcon = context.resources.getIdentifier(
            "launcher_icon",
            "mipmap",
            context.packageName
    )

    // Previous button
    private val prevIntent = Intent(context, NotificationReturnSlot::class.java).apply {
        action = "prev"
    }
    private val prevPendingIntent: PendingIntent =
            PendingIntent.getBroadcast(context, 0, prevIntent, 0)

    // Next button
    private val nextIntent = Intent(context, NotificationReturnSlot::class.java).apply {
        action = "next"
    }
    private val nextPendingIntent: PendingIntent =
            PendingIntent.getBroadcast(context, 0, nextIntent, 0)

    // Pause button
    private val pauseIntent = Intent(context, NotificationReturnSlot::class.java).apply {
        action = "pause"
    }
    private val pausePendingIntent: PendingIntent =
            PendingIntent.getBroadcast(context, 0, pauseIntent, 0)

    // Play button
    private val playIntent = Intent(context, NotificationReturnSlot::class.java).apply {
        action = "play"
    }
    private val playPendingIntent: PendingIntent =
            PendingIntent.getBroadcast(context, 0, playIntent, 0)

    init {
        Log.d("myvolumio", "MediaNotificationManager init")
        mediaSession.setMetadata(MediaMetadataCompat.fromMediaMetadata(mediaMetadata)) // remove seekbar
        builder = NotificationCompat.Builder(context, channelId)
                .setSmallIcon(smallIcon)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setNotificationSilent()
                .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                .setShowWhen(false)
                // Add media control buttons that invoke intents in your media service
                .addAction(R.drawable.ic_baseline_skip_previous_24, "Previous", prevPendingIntent) // #0
                .addAction(R.drawable.ic_baseline_pause_24, "Pause", pausePendingIntent) // #1
                .addAction(R.drawable.ic_baseline_play_arrow_24, "Play", playPendingIntent) // #2
                .addAction(R.drawable.ic_baseline_skip_next_24, "Next", nextPendingIntent) // #3

        createNotificationChannel()
    }

    private fun createNotificationChannel() {
        with(NotificationManagerCompat.from(context)) {
            // Create the NotificationChannel, but only on API 26+ because
            // the NotificationChannel class is new and not in the support library
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                val service: String = Context.NOTIFICATION_SERVICE

                val name = "MediaNotification"
                val descriptionText = "Media control notification"
                val importance = NotificationManager.IMPORTANCE_DEFAULT
                val channel = NotificationChannel(channelId, name, importance).apply {
                    description = descriptionText
                }

                val notificationManager: NotificationManager =
                        context.getSystemService(service) as NotificationManager
                notificationManager.createNotificationChannel(channel)
            }
        }
    }

    fun close() {
        with(NotificationManagerCompat.from(context)) {
            cancel(notificationId)
        }
    }

    fun sendNotification(song: String?, artist: String?, isPlaying: Boolean?, albumArt: String?) {

        loadAlbumArt(builder, albumArt)
        builder.setContentTitle(song)
        builder.setContentText(artist)

        // Apply the media style template
        if (isPlaying!!) {
            builder.setStyle(androidx.media.app.NotificationCompat.MediaStyle()
                    .setShowActionsInCompactView(0, 1, 3)
                    .setMediaSession(mediaSession.sessionToken))
        } else {
            builder.setStyle(androidx.media.app.NotificationCompat.MediaStyle()
                    .setShowActionsInCompactView(0, 2, 3)
                    .setMediaSession(mediaSession.sessionToken))
        }

        with(NotificationManagerCompat.from(context)) {
            notify(notificationId, builder.build())
        }
    }

    private fun loadAlbumArt(builder: NotificationCompat.Builder, albumArt: String?) = runBlocking {
        val url = URL(albumArt)

        withContext(Dispatchers.IO) {
            try {
                val input = url.openStream()
                BitmapFactory.decodeStream(input)
            } catch (e: IOException) {
                Log.e("myvolumio", e.message)
                null
            }
        }?.let { bitmap ->
            builder.setLargeIcon(bitmap)
        }
    }
}