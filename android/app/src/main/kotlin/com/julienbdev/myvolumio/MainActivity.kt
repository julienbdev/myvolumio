package com.julienbdev.myvolumio

import androidx.annotation.NonNull
import io.flutter.embedding.android.FlutterActivity
import io.flutter.embedding.engine.FlutterEngine
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugins.GeneratedPluginRegistrant

class MainActivity : FlutterActivity() {

    // channel is "static" to access it from NotificationReturSlot (for flutter callback)
    companion object {
        lateinit var channel: MethodChannel
    }

    lateinit var mediaNotificationManager: MediaNotificationManager

    override fun configureFlutterEngine(@NonNull flutterEngine: FlutterEngine) {
        GeneratedPluginRegistrant.registerWith(flutterEngine)
        mediaNotificationManager = MediaNotificationManager(this.context)
        val methodChannelName = "notificationPlugin" //set this string as you want. But keep it the same on the whole project.
        channel = MethodChannel(flutterEngine.dartExecutor.binaryMessenger, methodChannelName)

        channel.setMethodCallHandler { call, result ->
            when (call.method) {
                "getPlatformVersion" -> {
                    result.success("Android ${android.os.Build.VERSION.RELEASE}")
                }
                "showNotification" -> {
                    val song = call.argument<String>("song")
                    val artist = call.argument<String>("artist")
                    val isPlaying = call.argument<Boolean>("isPlaying")
                    val albumArt = call.argument<String>("albumArt")
                    mediaNotificationManager.sendNotification(song, artist, isPlaying, albumArt)
                }
                else -> {
                    result.notImplemented()
                }
            }
        }
    }

    override fun onDestroy() {
        mediaNotificationManager.close()
        super.onDestroy()
    }
}
