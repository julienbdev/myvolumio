class RouteNameUtility {
  static const login = 'login';
  static const signup = 'signup';
  static const search = 'search';
  static const settings = 'settings';
  static const player = 'player';
  static const home = 'home';
  static const artist = 'artist';
  static const album = 'album';
  static const playlist = 'playlist';
  static const playlists = 'playlists';
  static const library = 'library';
  static const queue = 'queue';
  static const addPlaylist = 'addPlaylist';
  static const root = '/';
}
