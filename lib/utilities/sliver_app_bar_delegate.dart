import 'package:flutter/material.dart';

class SliverAppBarDelegate extends SliverPersistentHeaderDelegate {
  final Widget _displayedWidget;
  final double _height;

  SliverAppBarDelegate(this._displayedWidget, this._height);

  @override
  Widget build(BuildContext context, double shrinkOffset, bool overlapsContent) {
    return Container(
      child: _displayedWidget,
    );
  }

  @override
  double get maxExtent => _height + 2;

  @override
  double get minExtent => _height + 2;

  @override
  bool shouldRebuild(covariant SliverPersistentHeaderDelegate oldDelegate) {
    return false;
  }
}