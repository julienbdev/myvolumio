import 'package:flutter/material.dart';
import 'package:logging/logging.dart';

class WidgetUtility {
  static final logger = Logger('WidgetUtility');

  static Widget buildAlbumArtCircle(String albumArt, String volumioHost) {
    return CircleAvatar(
      backgroundImage: AssetImage('assets/images/albumart.png'),
      child: ClipOval(
        child: FadeInImage.assetNetwork(
          fadeOutDuration: new Duration(milliseconds: 100),
          image: buildAlbumArtUrl(albumArt, volumioHost),
          placeholder: 'assets/images/albumart.png',
        ),
      ),
    );
  }

  static Widget buildAlbumArt(String albumArt, String volumioHost) {
    if (albumArt != null && albumArt.isNotEmpty) {
      return AspectRatio(
        aspectRatio: 1,
        child: FadeInImage.assetNetwork(
          fit: BoxFit.cover,
          fadeOutDuration: new Duration(milliseconds: 100),
          image: buildAlbumArtUrl(albumArt, volumioHost),
          placeholder: 'assets/images/albumart.png',
        ),
      );
    } else {
      return AspectRatio(
        aspectRatio: 1,
        child: Image.asset(
          'assets/images/albumart.png',
          fit: BoxFit.cover,
        ),
      );
    }
  }

  static String buildAlbumArtUrl(String albumArt, String volumioHost) {
    String albumArtUrl = '';

    if (albumArt.startsWith('http')) {
      return albumArt;
    }

    if (!volumioHost.startsWith('http')) {
      albumArtUrl = 'http://';
    }

    albumArtUrl += volumioHost;
    albumArtUrl += albumArt;

    return albumArtUrl;
  }

  static Widget buildPlaylistAlbumArt(List<String> urls, String volumioHost) {
    if (urls.length > 0) {
      if (urls.length < 4) {
        return Container(
          child: buildAlbumArt(urls.first, volumioHost),
        );
      } else {
        return GridView.count(
          physics: NeverScrollableScrollPhysics(),
          crossAxisCount: 2,
          children: List.generate(
            urls.length,
            (index) {
              return buildAlbumArt(urls[index], volumioHost);
            },
          ),
        );
      }
    } else {
      return Container(
        child: buildAlbumArt(null, volumioHost),
      );
    }
  }

  static List<String> buildPlaylistAlbumArtUrls(List<dynamic> data) {
    if (data != null && data.isNotEmpty) {
      List<String> urls = List<String>.from(data.map((e) => e['albumart']).toSet().toList());
      urls.removeWhere((element) => element == null);

      if (urls.length >= 4) {
        return urls.sublist(0, 4);
      } else {
        return urls.sublist(0, 1);
      }
    }

    return [];
  }
}
