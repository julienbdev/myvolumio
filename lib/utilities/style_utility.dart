import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class StyleUtility {
  static BoxDecoration get boxDecorationStyle {
    return BoxDecoration(
      color: Colors.grey[800],
      borderRadius: BorderRadius.circular(10.0),
      boxShadow: [
        BoxShadow(
          color: Colors.black12,
          blurRadius: 6.0,
          offset: Offset(0, 2),
        ),
      ],
    );
  }

  static Widget buildBackground() {
    return Container(
      height: double.infinity,
      width: double.infinity,
      decoration: BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          colors: [
            Colors.grey[850],
            Colors.grey[900],
          ],
        ),
      ),
    );
  }

  static ThemeData get darkTheme {
    // make sur navigation bar is also dark
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      systemNavigationBarColor: Colors.black, // navigation bar color
      systemNavigationBarDividerColor: Colors.tealAccent, //Navigation bar divider color
      systemNavigationBarIconBrightness: Brightness.light, //navigation bar icon
    ));

    return ThemeData.dark().copyWith(
      buttonTheme: ButtonThemeData(buttonColor: Color(0xff00BD5F)),
      colorScheme: ThemeData.dark().colorScheme.copyWith(
            primary: Colors.tealAccent,
          ),
      textSelectionTheme: TextSelectionThemeData(
        cursorColor: Colors.tealAccent,
      ),
      toggleableActiveColor: Color(0xff00BD5F),
      snackBarTheme: SnackBarThemeData(
        backgroundColor: Colors.grey[800],
        contentTextStyle: TextStyle(color: Colors.white),
      ),
      inputDecorationTheme: InputDecorationTheme(
        contentPadding: EdgeInsets.all(0),
        filled: true,
        fillColor: Colors.grey[800],
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(10.0),
          borderSide: BorderSide.none,
        ),
      ),
    );
  }

  static ThemeData get lightTheme {
    return ThemeData.light();
  }

  static String formatDuration(Duration d) {
    return d.toString().substring(2, 7);
  }
}
