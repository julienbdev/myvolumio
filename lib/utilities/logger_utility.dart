import 'dart:developer' as developer;
import 'package:flutter/foundation.dart';
import 'package:intl/intl.dart';
import 'package:logging/logging.dart';

class LoggerUtility {
  static void initRootLogger() {
    // only enable logging for debug mode
    if (kDebugMode) {
      Logger.root.level = Level.CONFIG;
    } else {
      Logger.root.level = Level.OFF;
    }
    hierarchicalLoggingEnabled = true;

    // specify the levels for lower level loggers, if desired
    // Logger('SiteInfoService').level = Level.ALL;

    Logger.root.onRecord.listen(
      (record) {
        if (!kDebugMode) {
          return;
        }

        var start = '\x1b[90m';
        const end = '\x1b[0m';
        const white = '\x1b[37m';

        switch (record.level.name) {
          case 'CONFIG':
            start = '\x1b[32m';
            break;
          case 'INFO':
            start = '\x1b[37m';
            break;
          case 'WARNING':
            start = '\x1b[93m';
            break;
          case 'SEVERE':
            start = '\x1b[103m\x1b[31m';
            break;
          case 'SHOUT':
            start = '\x1b[41m\x1b[93m';
            break;
        }

        final String time = DateFormat('dd/MM/yyyy kk:mm:ss').format(record.time);

        final message = '$white$time : $end$start${record.level.name.padRight(7)} : ${record.message}$end';
        developer.log(
          message,
          name: 'L/${record.loggerName.padRight(25)}',
          level: record.level.value,
          time: record.time,
        );
      },
    );
  }
}
