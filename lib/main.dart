import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:get_it/get_it.dart';
import 'package:logging/logging.dart';
import 'package:myvolumio/models/volumio_user.dart';
import 'package:myvolumio/screens/addPlaylist/add_playlist_screen.dart';
import 'package:myvolumio/screens/config_screen.dart';
import 'package:myvolumio/screens/home_screen.dart';
import 'package:myvolumio/screens/loading_screen.dart';
import 'package:myvolumio/screens/login_screen.dart';
import 'package:myvolumio/screens/player/player_screen.dart';
import 'package:myvolumio/screens/queue/queue_screen.dart';
import 'package:myvolumio/screens/signup_screen.dart';
import 'package:myvolumio/services/navigation_service.dart';
import 'package:myvolumio/services/player_service.dart';
import 'package:myvolumio/services/socket_service.dart';
import 'package:myvolumio/services/stats_service.dart';
import 'package:myvolumio/services/user_service.dart';
import 'package:myvolumio/utilities/logger_utility.dart';
import 'package:myvolumio/utilities/route_name_utility.dart';
import 'package:myvolumio/utilities/style_utility.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'app_localizations.dart';

void main() {
  LoggerUtility.initRootLogger();
  final locator = GetIt.instance;
  locator.registerSingleton<PlayerService>(PlayerService());
  locator.registerSingleton<UserService>(UserService());
  locator.registerSingleton<StatsService>(StatsService());
  locator.registerSingleton<SocketService>(SocketService());
  locator.registerSingleton<NavigationService>(NavigationService());

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  static final logger = Logger('Main');
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    final Future<FirebaseApp> _firebaseInit = Firebase.initializeApp();
    final Future<SharedPreferences> _prefsInit = SharedPreferences.getInstance();

    return FutureBuilder(
      future: Future.wait([_firebaseInit, _prefsInit]),
      builder: (context, initSnapshot) {
        return MaterialApp(
          title: 'My Volumio',
          localizationsDelegates: [
            // Class which loads the translations from JSON files
            AppLocalizations.delegate,
            // Built-in localization of basic text for Material widgets
            GlobalMaterialLocalizations.delegate,
            // Built-in localization for text direction LTR/RTL
            GlobalWidgetsLocalizations.delegate,
            // Built-in localization of basic text for iOS widgets
            GlobalCupertinoLocalizations.delegate,
          ],
          supportedLocales: [
            const Locale('en', ''),
            const Locale('fr', ''),
          ],
          // Returns a locale which will be used by the app
          localeResolutionCallback: (locale, supportedLocales) {
            // Check if the current device locale is supported
            for (var supportedLocale in supportedLocales) {
              if (supportedLocale.languageCode == locale.languageCode) {
                return supportedLocale;
              }
            }
            // If the locale of the device is not supported, use the first one
            // from the list (English, in this case).
            return supportedLocales.first;
          },
          theme: StyleUtility.lightTheme,
          darkTheme: StyleUtility.darkTheme,
          themeMode: ThemeMode.dark, // dark theme only
          home: _buildHomeWidget(initSnapshot),
          routes: {
            RouteNameUtility.login: (ctx) => LoginScreen(),
            RouteNameUtility.signup: (ctx) => SignupScreen(),
            RouteNameUtility.home: (ctx) => HomeScreen(),
            RouteNameUtility.settings: (ctx) => ConfigScreen(),
            RouteNameUtility.player: (ctx) => PlayerScreen(),
            RouteNameUtility.queue: (ctx) => QueueScreen(),
            RouteNameUtility.addPlaylist: (ctx) => AddPlaylistScreen(),
          },
        );
      },
    );
  }

  VolumioUser _buildVolumioUser(DocumentSnapshot<Map<String, dynamic>> userDocument) {
    VolumioUser user = VolumioUser();
    user.name = userDocument.data()['name'];
    user.email = userDocument.data()['email'];
    user.id = userDocument.id;
    user.volumioHost = userDocument.data()['volumioHost'];
    user.volumioPort = userDocument.data()['volumioPort'];

    return user;
  }

  void _updateVolumioUser(BuildContext context, DocumentSnapshot userDoc) async {
    logger.info('_updateVolumioUser -> uid : ' + userDoc.id);
    VolumioUser user = _buildVolumioUser(userDoc);
    logger.info('_updateVolumioUser -> user : $user');

    final locator = GetIt.instance;
    locator.get<UserService>().udpateUser(user);
  }

  Widget _buildHomeWidget(AsyncSnapshot<dynamic> initSnapshot) {
    if (initSnapshot.connectionState != ConnectionState.done) {
      return LoadingScreen();
    } else {
      SharedPreferences prefs = initSnapshot.data[1];
      if (prefs.containsKey('rememberMe')) {
        final rememberMe = prefs.getBool('rememberMe');
        if (rememberMe) {
          return StreamBuilder(
            stream: FirebaseAuth.instance.authStateChanges(),
            builder: (context, AsyncSnapshot<User> firebaseAuthSnapshot) {
              if (firebaseAuthSnapshot.connectionState == ConnectionState.waiting) {
                logger.info('Main -> LoadingScreen');
                return LoadingScreen();
              }
              if (firebaseAuthSnapshot.hasData) {
                return FutureBuilder(
                  future: FirebaseFirestore.instance.collection('users').doc(firebaseAuthSnapshot.data.uid).get(), //_updateVolumioUser(context, firebaseAuthSnapshot.data.uid),
                  builder: (context, snapshot) {
                    if (!snapshot.hasData) {
                      logger.info('_updateVolumioUser waiting, Main -> LoadingScreen');
                      return LoadingScreen();
                    } else {
                      logger.info('Main -> HomeScreen');
                      _updateVolumioUser(context, snapshot.data);
                      return HomeScreen();
                    }
                  },
                );
              }
              logger.info('Main -> LoginScreen');
              return LoginScreen();
            },
          );
        } else {
          logger.info('Main -> LoginScreen');
          return LoginScreen();
        }
      } else {
        // no pref for auto login => force login
        logger.info('Main -> LoginScreen');
        return LoginScreen();
      }
    }
  }
}
