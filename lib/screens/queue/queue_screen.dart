import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:logging/logging.dart';
import 'package:myvolumio/screens/queue/playing_widget.dart';
import 'package:myvolumio/screens/queue/queue_songs.dart';
import 'package:myvolumio/services/socket_service.dart';

class QueueScreen extends StatelessWidget {
  final SocketService socketService = GetIt.instance.get<SocketService>();
  static final logger = Logger('QueueScreen');

  @override
  Widget build(BuildContext context) {
    double horizontalPadding = (MediaQuery.of(context).size.width * 5) / 100;

    return Container(
      decoration: BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topLeft,
          end: Alignment.bottomRight,
          colors: [Colors.grey[800], Colors.black.withOpacity(0.6)],
        ),
      ),
      child: Scaffold(
        // By defaut, Scaffold background is white
        // Set its value to transparent
        backgroundColor: Colors.transparent,
        appBar: AppBar(
          backgroundColor: Colors.transparent,
          shadowColor: Colors.transparent,
          actions: [
            IconButton(
              icon: Icon(Icons.save),
              onPressed: () {},
            ),
            IconButton(
              icon: Icon(Icons.delete),
              onPressed: () {
                socketService.emitClearQueue();
              },
            ),
          ],
        ),
        body: Padding(
          padding: EdgeInsets.symmetric(horizontal: horizontalPadding),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              PlayingWidget(),
              QueueSongs(),
            ],
          ),
        ),
      ),
    );
  }
}
