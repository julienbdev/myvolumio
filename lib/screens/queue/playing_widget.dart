import 'package:flutter/material.dart';
import 'package:myvolumio/app_localizations.dart';
import 'package:myvolumio/screens/queue/playing_album_art_widget.dart';
import 'package:myvolumio/screens/queue/playing_artist_text_widget.dart';
import 'package:myvolumio/screens/queue/playing_song_text_widget.dart';

class PlayingWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          AppLocalizations.of(context).translate('playing'),
          style: TextStyle(
            fontSize: Theme.of(context).textTheme.subtitle1.fontSize,
            fontWeight: FontWeight.bold,
          ),
        ),
        ListTile(
          contentPadding: EdgeInsets.zero,
          leading: PlayingAlbumArtWidget(),
          title: PlayingSongTextWidget(),
          subtitle: PlayingArtistTextWidget(),
        )
      ],
    );
  }
}
