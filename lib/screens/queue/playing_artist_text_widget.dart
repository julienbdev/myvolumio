import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:myvolumio/services/player_service.dart';

class PlayingArtistTextWidget extends StatelessWidget {
  final PlayerService _playerService = GetIt.instance.get<PlayerService>();

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<String>(
      stream: _playerService.artistStream,
      initialData: _playerService.lastArtist,
      builder: (context, snapshot) {
        return Text(
          snapshot.data,
          overflow: TextOverflow.fade,
          softWrap: false,
        );
      },
    );
  }
}
