import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:get_it/get_it.dart';
import 'package:logging/logging.dart';
import 'package:myvolumio/app_localizations.dart';
import 'package:myvolumio/services/player_service.dart';
import 'package:myvolumio/services/socket_service.dart';
import 'package:myvolumio/utilities/route_name_utility.dart';

class QueueSongs extends StatefulWidget {
  @override
  _QueueSongsState createState() => _QueueSongsState();
}

class _QueueSongsState extends State<QueueSongs> {
  final _locator = GetIt.instance;
  SocketService _socketService;
  PlayerService _playerService;
  static final logger = Logger('QueueScreen');
  String _uri = '';
  final SlidableController slidableController = SlidableController();

  @override
  initState() {
    super.initState();
    _socketService = _locator.get<SocketService>();
    _playerService = _locator.get<PlayerService>();
    _socketService.emitGetQueue();

    _uri = _playerService.lastUri;
    _playerService.uriStream.listen((value) {
      if (!mounted) return;

      logger.info('uri : $value');
      setState(() {
        _uri = value;
      });
    });
  }

  Widget _buildQueue(BuildContext context) {
    return Expanded(
      child: StreamBuilder<dynamic>(
          stream: _socketService.getQueueStream(),
          builder: (context, snapshot) {
            List<dynamic> results = [];
            if (snapshot.hasData) {
              results = snapshot.data;
            }

            return ListView.builder(
              itemCount: results.length,
              itemBuilder: (BuildContext context, int index) {
                return Slidable(
                  key: UniqueKey(),
                  actionPane: SlidableDrawerActionPane(),
                  controller: slidableController,
                  actions: [
                    IconSlideAction(
                      color: Colors.transparent,
                      icon: Icons.delete,
                      onTap: () {
                        _socketService.emitRemoveFromQueue(index);
                      },
                    ),
                    IconSlideAction(
                      color: Colors.transparent,
                      icon: Icons.playlist_add,
                      onTap: () {
                        Navigator.of(context).pushNamed(RouteNameUtility.addPlaylist, arguments: results[index]);
                      },
                    )
                  ],
                  child: ListTile(
                    key: UniqueKey(),
                    contentPadding: EdgeInsets.zero,
                    title: Text(
                      results[index]['name'],
                      overflow: TextOverflow.fade,
                      softWrap: false,
                    ),
                    subtitle: Text(
                      results[index]['artist'],
                      overflow: TextOverflow.fade,
                      softWrap: false,
                    ),
                    onTap: () {
                      _socketService.emitPlay(index);
                    },
                    trailing: _uri == results[index]['uri'] ? Icon(Icons.play_circle_fill) : null,
                  ),
                );
              },
            );
          }),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 20),
            child: Text(AppLocalizations.of(context).translate('queue'),
                style: TextStyle(
                  fontSize: Theme.of(context).textTheme.subtitle1.fontSize,
                  fontWeight: FontWeight.bold,
                )),
          ),
          _buildQueue(context),
        ],
      ),
    );
  }
}
