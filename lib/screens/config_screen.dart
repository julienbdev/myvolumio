import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_html/style.dart';
import 'package:get_it/get_it.dart';
import 'package:logging/logging.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:myvolumio/app_localizations.dart';
import 'package:myvolumio/models/volumio_user.dart';
import 'package:myvolumio/services/socket_service.dart';
import 'package:myvolumio/services/user_service.dart';
import 'package:myvolumio/utilities/route_name_utility.dart';

class ConfigScreen extends StatefulWidget {
  @override
  _ConfigScreenState createState() => _ConfigScreenState();
}

class _ConfigScreenState extends State<ConfigScreen> {
  final locator = GetIt.instance;
  final SocketService socketService = GetIt.instance.get<SocketService>();
  int defaultPort = 3000;
  VolumioUser _connectedUser;
  TextEditingController _volumioHostController = TextEditingController();
  TextEditingController _volumioPortController = TextEditingController();
  static final logger = Logger('ConfigScreen');

  Widget _buildHostInput() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          AppLocalizations.of(context).translate('volumioHost'),
        ),
        SizedBox(height: 10),
        TextField(
          textAlignVertical: TextAlignVertical.center,
          keyboardType: TextInputType.url,
          controller: _volumioHostController,
          decoration: InputDecoration(
            prefixIcon: Icon(
              Icons.computer_rounded,
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildPortInput() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          AppLocalizations.of(context).translate('volumioPort'),
        ),
        SizedBox(height: 10),
        TextField(
          textAlignVertical: TextAlignVertical.center,
          keyboardType: TextInputType.number,
          controller: _volumioPortController,
          decoration: InputDecoration(
            prefixIcon: Icon(
              Icons.build,
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildName() {
    return ListTile(
      contentPadding: EdgeInsets.zero,
      leading: Icon(Icons.account_circle),
      title: Text(_connectedUser.name),
    );
  }

  Widget _buildCredits() {
    return ListTile(
      contentPadding: EdgeInsets.zero,
      title: Text(AppLocalizations.of(context).translate('credits')),
      subtitle: Html(
        style: {
          "body": Style(margin: EdgeInsets.all(0)), // <-- remove the margin
        },
        data: AppLocalizations.of(context).translate('appIconCredit'),
        onLinkTap: (url, context, attributes, element) {
          launch(url);
        },
      ),
    );
  }

  Widget _buildLogout() {
    return ListTile(
      contentPadding: EdgeInsets.zero,
      title: Text(AppLocalizations.of(context).translate('logout')),
      subtitle: Text(AppLocalizations.of(context).translate('connectedAs') + ' ' + _connectedUser.name),
      onTap: () {
        _logout();
        Navigator.of(context).pushNamedAndRemoveUntil(RouteNameUtility.login, ModalRoute.withName('/'));
      },
    );
  }

  Future<bool> _onBackPressed() async {
    logger.info('back pressed');
    // Save user settings
    locator.get<UserService>().updateHost(_volumioHostController.text);
    locator.get<UserService>().updatePort(int.tryParse(_volumioPortController.text));

    await FirebaseFirestore.instance.collection('users').doc(_connectedUser.id).update(
      {'volumioHost': _volumioHostController.text, 'volumioPort': int.tryParse(_volumioPortController.text)},
    );

    return true;
  }

  void _logout() async {
    socketService.disconnect();
    await FirebaseAuth.instance.signOut();
  }

  @override
  Widget build(BuildContext context) {
    _connectedUser = locator.get<UserService>().getUser;
    _volumioHostController.text = _connectedUser.volumioHost;
    _volumioPortController.text = _connectedUser.volumioPort != null ? _connectedUser.volumioPort.toString() : defaultPort.toString();

    return WillPopScope(
      onWillPop: _onBackPressed,
      child: Scaffold(
        appBar: AppBar(
          title: Text(AppLocalizations.of(context).translate('settings')),
        ),
        body: SingleChildScrollView(
          padding: EdgeInsets.symmetric(horizontal: (MediaQuery.of(context).size.width * 7) / 100),
          child: Column(
            children: [
              _buildName(),
              SizedBox(height: 10),
              _buildHostInput(),
              SizedBox(height: 10),
              _buildPortInput(),
              SizedBox(height: 10),
              _buildCredits(),
              //SizedBox(height: 10),
              _buildLogout(),
            ],
          ),
        ),
      ),
    );
  }
}
