import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:logging/logging.dart';
import 'package:myvolumio/app_localizations.dart';
import 'package:myvolumio/plugins/notification_plugin.dart';
import 'package:myvolumio/services/navigation_service.dart';
import 'package:myvolumio/services/socket_service.dart';
import 'package:myvolumio/services/stats_service.dart';
import 'package:myvolumio/widgets/player_widget.dart';
import 'package:myvolumio/widgets/progress_widget.dart';
import 'package:socket_io_client/socket_io_client.dart' as IO;
import 'package:myvolumio/models/volumio_user.dart';
import 'package:myvolumio/pages/home_page.dart';
import 'package:myvolumio/pages/library_page.dart';
import 'package:myvolumio/pages/search_page.dart';
import 'package:myvolumio/services/user_service.dart';
import 'package:myvolumio/utilities/route_name_utility.dart';

class TabNavigationItem {
  final Widget page;
  final String title;
  final Icon icon;

  TabNavigationItem({
    @required this.page,
    @required this.title,
    @required this.icon,
  });
}

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  final locator = GetIt.instance;
  int _currentIndex = 0;
  VolumioUser _connectedUser;
  bool _socketConnected = false;
  IO.Socket socket;
  SocketService socketService;
  UserService userService;
  NavigationService navigationService;
  StatsService statsService;
  static final logger = Logger('HomeScreen');

  @override
  initState() {
    super.initState();
    statsService = locator.get<StatsService>();
    socketService = locator.get<SocketService>();
    userService = locator.get<UserService>();
    navigationService = locator.get<NavigationService>();
    navigationService.currentPage = 0;
    statsService.init(userService.getUser.id, userService.getUser.volumioHost, FirebaseFirestore.instance);
    NotificationPlugin.init(userService.getUser.volumioHost);
  }

  static List<TabNavigationItem> getTabNavigationItems(BuildContext context) => [
        TabNavigationItem(
          page: HomePage(),
          icon: Icon(Icons.home),
          title: AppLocalizations.of(context).translate('home'),
        ),
        TabNavigationItem(
          page: SearchPage(),
          icon: Icon(Icons.search),
          title: AppLocalizations.of(context).translate('toSearch'),
        ),
        TabNavigationItem(
          page: LibraryPage(),
          icon: Icon(Icons.my_library_books),
          title: AppLocalizations.of(context).translate('library'),
        ),
      ];

  Widget _buildConfigureWidget(BuildContext context) {
    return Container(
      width: double.infinity,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(AppLocalizations.of(context).translate('configureVolumio')),
          RaisedButton(
            child: Text(AppLocalizations.of(context).translate('configure')),
            onPressed: () {
              Navigator.of(context).pushNamed(RouteNameUtility.settings).then((value) => setState(() {
                    //_socketConnected = socketService.isConnected();
                  }));
            },
          ),
        ],
      ),
    );
  }

  Widget _buildConnectedHomePage(BuildContext context) {
    return AnimatedSwitcher(
      transitionBuilder: AnimatedSwitcher.defaultTransitionBuilder,
      duration: const Duration(milliseconds: 500),
      child: IndexedStack(
        key: ValueKey<int>(_currentIndex),
        index: _currentIndex,
        children: [
          for (final tabItem in getTabNavigationItems(context)) tabItem.page,
        ],
      ),
    );
  }

  Widget _buildScreenWidget(BuildContext context) {
    if (_connectedUser.isConfigured()) {
      logger.info('user is configured');
      if (_socketConnected) {
        logger.info('_socketConnected true');
        return _buildConnectedHomePage(context);
      } else {
        // Connect socket
        logger.info('Connect socket');
        socket = socketService.connect(_connectedUser.getSocketUrl());
        return StreamBuilder<dynamic>(
          stream: socketService.getConnectedStream(),
          builder: (context, snapshot) {
            logger.info(snapshot);
            if (snapshot.hasData && snapshot.data == true) {
              _socketConnected = true;

              return _buildConnectedHomePage(context);
            } else {
              return SafeArea(
                child: Stack(
                  children: [
                    Align(
                      alignment: Alignment.topRight,
                      child: IconButton(
                          icon: Icon(Icons.settings),
                          onPressed: () {
                            Navigator.of(context).pushNamed(RouteNameUtility.settings).then((value) => setState(() {}));
                          }),
                    ),
                    Center(child: CircularProgressIndicator()),
                  ],
                ),
              );
            }
          },
        );
      }
    } else {
      logger.info('user not configured');
      return _buildConfigureWidget(context);
    }
  }

  void _backIndex() {
    navigationService.backIndex();
    setState(() {
      _currentIndex = navigationService.currentPage;
    });
  }

  Future<bool> _onBackPressed() async {
    logger.info('home screen back pressed');
    logger.info('canPop navigationKey: ${navigationService.navigatorKey.currentState.canPop()}');
    logger.info('navigationService.navigatorKey.currentState: ${navigationService.navigatorKey.currentState.toString()}');
    if (navigationService.navigatorKey.currentState.canPop()) {
      navigationService.navigatorKey.currentState.pop();
    } else {
      _backIndex();
    }

    return false;
  }

  @override
  Widget build(BuildContext context) {
    logger.info('Home screen build');
    socket = socketService.socket;
    _connectedUser = userService.getUser;
    _socketConnected = socketService.isConnected();

    logger.info(_socketConnected ? 'socket connected' : 'socket not connected');

    return WillPopScope(
      onWillPop: _onBackPressed,
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        body: Column(
          children: [
            Expanded(
              flex: 9,
              child: _buildScreenWidget(context),
            ),
            ProgressWidget(),
            StreamBuilder<dynamic>(
                stream: socketService.getConnectedStream(),
                initialData: _socketConnected,
                builder: (context, snapshot) {
                  if (snapshot.hasData && snapshot.data == true) {
                    return Expanded(
                      flex: 1,
                      child: PlayerWidget(),
                    );
                  }
                  return Container();
                }),
          ],
        ),
        bottomNavigationBar: BottomNavigationBar(
          currentIndex: _currentIndex,
          onTap: (int index) {
            // History for back button
            if (index != _currentIndex) {
              navigationService.history.add(index);
              // update current tab screen
              setState(() {
                navigationService.currentPage = index;
                _currentIndex = index;
              });
            }
          },
          items: [
            for (final tabItem in getTabNavigationItems(context))
              BottomNavigationBarItem(
                icon: tabItem.icon,
                label: tabItem.title,
              )
          ],
        ),
      ),
    );
  }
}
