import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:logging/logging.dart';
import 'package:myvolumio/app_localizations.dart';
import 'package:myvolumio/models/volumio_user.dart';
import 'package:myvolumio/services/stats_service.dart';
import 'package:myvolumio/services/user_service.dart';
import 'package:myvolumio/utilities/style_utility.dart';
import 'package:myvolumio/utilities/route_name_utility.dart';
import 'package:email_validator/email_validator.dart';

class SignupScreen extends StatefulWidget {
  @override
  _SignupScreenState createState() => _SignupScreenState();
}

class _SignupScreenState extends State<SignupScreen> {
  final locator = GetIt.instance;
  final _scaffoldKey = new GlobalKey<ScaffoldState>();
  final _auth = FirebaseAuth.instance;
  final _formKey = GlobalKey<FormState>();
  String _email;
  String _password;
  String _name;
  bool _isLoading = false;
  StatsService statsService;
  static final logger = Logger('SignupScreen');

  TextEditingController _passwordController = TextEditingController();

  @override
  void initState() {
    super.initState();
    statsService = locator.get<StatsService>();
  }

  void _submitForm() {
    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();
      _signUp(_email, _password, _name);
    }
  }

  VolumioUser _buildVolumioUser(DocumentSnapshot<Map<String, dynamic>> userDocument) {
    VolumioUser user = VolumioUser();
    user.name = userDocument.data()['name'];
    user.email = userDocument.data()['email'];
    user.id = userDocument.id;
    user.volumioHost = userDocument.data()['volumioHost'];
    user.volumioPort = userDocument.data()['volumioPort'];

    return user;
  }

  _signUp(String email, String password, String name) async {
    setState(() {
      _isLoading = true;
    });
    try {
      UserCredential authResult = await _auth.createUserWithEmailAndPassword(email: email, password: password);
      await FirebaseFirestore.instance.collection('users').doc(authResult.user.uid).set({
        'name': name,
        'email': email,
      });

      // Build user for home screen
      DocumentSnapshot userDocument = await FirebaseFirestore.instance.collection('users').doc(authResult.user.uid).get();
      VolumioUser user = _buildVolumioUser(userDocument);
      // save connected user in memory
      locator.get<UserService>().udpateUser(user);
      statsService.init(user.id, user.volumioHost, FirebaseFirestore.instance);
      await statsService.initStats();

      // User is logged in after user creation (Firebase design)
      Navigator.of(context).pushNamedAndRemoveUntil(
        RouteNameUtility.home,
        ModalRoute.withName('/'),
      );
    } on FirebaseAuthException catch (err) {
      setState(() {
        _isLoading = false;
      });
      logger.severe(err);
      _scaffoldKey.currentState.showSnackBar(
        SnackBar(
          content: Text(AppLocalizations.of(context).translate(err.code)),
        ),
      );
    } catch (err) {
      setState(() {
        _isLoading = false;
      });
      logger.severe(err);
      _scaffoldKey.currentState.showSnackBar(
        SnackBar(
          content: Text(AppLocalizations.of(context).translate('errorOccured')),
        ),
      );
    }
  }

  String _validateEmail(String email) {
    if (email.isEmpty) {
      return AppLocalizations.of(context).translate('emailMandatory');
    }
    if (!EmailValidator.validate(email)) {
      return AppLocalizations.of(context).translate('emailNotValid');
    }

    return null;
  }

  String _validatePassword(String password) {
    if (password.isEmpty) {
      return AppLocalizations.of(context).translate('passwordMandatory');
    }

    return null;
  }

  String _validateConfirmPassword(String confirmPassword, password) {
    if (confirmPassword.isEmpty) {
      return AppLocalizations.of(context).translate('confirmPasswordMandatory');
    }

    if (confirmPassword != password) {
      return AppLocalizations.of(context).translate('confirmPasswordNoMatch');
    }

    return null;
  }

  String _validateName(String name) {
    if (name.isEmpty) {
      return AppLocalizations.of(context).translate('nameMandatory');
    }

    return null;
  }

  Widget _buildImageLogo() {
    return Image.asset(
      'assets/images/volumio-logo.png',
      fit: BoxFit.contain,
    );
  }

  Widget _buildNameInput() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          AppLocalizations.of(context).translate('name'),
        ),
        SizedBox(height: 10),
        TextFormField(
          keyboardType: TextInputType.text,
          textAlignVertical: TextAlignVertical.center,
          validator: (value) => _validateName(value),
          onSaved: (val) => _name = val,
          decoration: InputDecoration(
            prefixIcon: Icon(Icons.person_rounded),
            hintText: AppLocalizations.of(context).translate('nameHint'),
          ),
        ),
      ],
    );
  }

  Widget _buildEmailInput() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          AppLocalizations.of(context).translate('email'),
        ),
        SizedBox(height: 10),
        TextFormField(
          keyboardType: TextInputType.emailAddress,
          textAlignVertical: TextAlignVertical.center,
          validator: (value) => _validateEmail(value),
          onSaved: (val) => _email = val,
          decoration: InputDecoration(
            prefixIcon: Icon(
              Icons.email,
            ),
            hintText: AppLocalizations.of(context).translate('emailHint'),
          ),
        ),
      ],
    );
  }

  Widget _buildPasswordInput() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          AppLocalizations.of(context).translate('password'),
        ),
        SizedBox(height: 10),
        TextFormField(
          controller: _passwordController,
          obscureText: true,
          textAlignVertical: TextAlignVertical.center,
          validator: (value) => _validatePassword(value),
          onSaved: (val) => _password = val,
          decoration: InputDecoration(
            prefixIcon: Icon(
              Icons.lock,
            ),
            hintText: AppLocalizations.of(context).translate('passwordHint'),
          ),
        ),
      ],
    );
  }

  Widget _buildConfirmPasswordInput() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          AppLocalizations.of(context).translate('confirmPassword'),
        ),
        SizedBox(height: 10),
        TextFormField(
          obscureText: true,
          textAlignVertical: TextAlignVertical.center,
          validator: (value) => _validateConfirmPassword(value, _passwordController.text),
          decoration: InputDecoration(
            prefixIcon: Icon(
              Icons.lock,
            ),
            hintText: AppLocalizations.of(context).translate('confirmPasswordHint'),
          ),
        ),
      ],
    );
  }

  Widget _buildRegisterButton() {
    double padding = 15.0;
    double fontSize = Theme.of(context).textTheme.headline5.fontSize;
    double loadingSize = fontSize + (padding / 2);

    return Container(
      width: double.infinity,
      child: RaisedButton(
        elevation: 5.0,
        onPressed: () => _submitForm(),
        padding: EdgeInsets.all(padding),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(30.0),
        ),
        child: _isLoading
            ? SizedBox(
                height: loadingSize,
                width: loadingSize,
                child: CircularProgressIndicator(
                  valueColor: new AlwaysStoppedAnimation<Color>(
                    Colors.white,
                  ),
                ),
              )
            : Text(
                AppLocalizations.of(context).translate('registerAccount'),
                style: TextStyle(
                  fontSize: fontSize,
                  fontWeight: FontWeight.bold,
                ),
              ),
      ),
    );
  }

  Widget _buildSigninButton() {
    return Container(
      alignment: Alignment.bottomCenter,
      child: FlatButton(
          onPressed: () => Navigator.of(context).pushNamed(RouteNameUtility.login),
          padding: EdgeInsets.only(right: 0.0),
          child: Column(
            children: [
              Text(
                AppLocalizations.of(context).translate('alreadyAccount'),
              ),
              Text(
                AppLocalizations.of(context).translate('signin'),
              ),
            ],
          )),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      body: Stack(
        children: [
          StyleUtility.buildBackground(),
          Container(
            height: double.infinity,
            width: double.infinity,
            child: SingleChildScrollView(
              padding: EdgeInsets.fromLTRB(
                (MediaQuery.of(context).size.width * 7) / 100, // left
                (MediaQuery.of(context).size.height * 15) / 100, // top
                (MediaQuery.of(context).size.width * 7) / 100, // right
                (MediaQuery.of(context).size.height * 0) / 100, // bottom
              ),
              child: Form(
                key: _formKey,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    _buildImageLogo(),
                    SizedBox(height: 30),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        _buildNameInput(),
                        SizedBox(height: 15),
                        _buildEmailInput(),
                        SizedBox(height: 15),
                        _buildPasswordInput(),
                        SizedBox(height: 15),
                        _buildConfirmPasswordInput(),
                        SizedBox(height: 20),
                        _buildRegisterButton(),
                        SizedBox(height: 20),
                        _buildSigninButton(),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
