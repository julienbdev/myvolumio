import 'package:flutter/material.dart';
import 'package:myvolumio/app_localizations.dart';

class NewPlaylistScreen extends StatelessWidget {
  final TextEditingController _nameController = TextEditingController();

  String _validateName(String name) {
    if (name.isEmpty) {
      return AppLocalizations.instance.translate('nameMandatory');
    }

    return null;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topLeft,
          end: Alignment.bottomRight,
          colors: [Colors.grey[800], Colors.black.withOpacity(0.6)],
        ),
      ),
      child: Scaffold(
        // By defaut, Scaffold background is white
        // Set its value to transparent
        backgroundColor: Colors.transparent,
        body: Column(
          children: [
            Text(AppLocalizations.instance.translate('playlistName')),
            TextFormField(
              controller: _nameController,
              keyboardType: TextInputType.text,
              autocorrect: false,
              //textCapitalization: TextCapitalization.none,
              textAlignVertical: TextAlignVertical.center,
              validator: (value) => _validateName(value),
            ),
            Row(
              children: [
                ElevatedButton(
                  onPressed: () {},
                  child: Text(AppLocalizations.instance.translate('cancel')),
                ),
                ElevatedButton(
                  onPressed: () {},
                  child: Text(AppLocalizations.instance.translate('validate')),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
