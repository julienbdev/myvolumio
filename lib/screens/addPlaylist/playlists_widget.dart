import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:myvolumio/services/socket_service.dart';

class PlaylistsWidget extends StatefulWidget {
  final dynamic item;

  const PlaylistsWidget(this.item);

  @override
  _PlaylistsWidgetState createState() => _PlaylistsWidgetState();
}

class _PlaylistsWidgetState extends State<PlaylistsWidget> {
  SocketService _socketService = GetIt.instance.get<SocketService>();

  @override
  void initState() {
    super.initState();
    _socketService.emitPlaylists();
  }

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: StreamBuilder<dynamic>(
        stream: _socketService.getPlaylistsStream(),
        builder: (context, snapshot) {
          List<String> playlists = [];
          if (snapshot.hasData) {
            playlists = List<String>.from(snapshot.data);
          }
          return ListView.builder(
            itemCount: playlists.length,
            itemBuilder: (context, index) {
              return ListTile(
                leading: Icon(Icons.format_list_bulleted),
                title: Text('${playlists[index]}'),
                onTap: () {
                  _socketService.emitAddToPlaylist(playlists[index], widget.item['service'], widget.item['uri']);
                  Navigator.pop(context);
                },
              );
            },
          );
        },
      ),
    );
  }
}
