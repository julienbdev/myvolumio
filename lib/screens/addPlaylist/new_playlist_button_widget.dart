import 'package:flutter/material.dart';
import 'package:myvolumio/app_localizations.dart';

class NewPlaylistButtonWidget extends StatelessWidget {
  final dynamic item;

  const NewPlaylistButtonWidget(this.item);

  Widget _buildCreatePlaylistButton(BuildContext context, double buttonVerticalMargin, double buttonFontSize, double buttonPadding) {
    return Container(
      width: double.infinity,
      child: Padding(
        padding: EdgeInsets.fromLTRB(
          (MediaQuery.of(context).size.width * 10) / 100, // left
          buttonVerticalMargin, // top
          (MediaQuery.of(context).size.width * 10) / 100, // right
          buttonVerticalMargin, // bottom
        ),
        child: RaisedButton(
          padding: EdgeInsets.all(buttonPadding),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(30.0),
          ),
          child: Text(
            AppLocalizations.of(context).translate('createPlaylist').toUpperCase(),
            style: TextStyle(fontSize: buttonFontSize),
          ),
          onPressed: () {},
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    double _buttonVerticalMargin = (MediaQuery.of(context).size.height * 2) / 100;
    double _buttonFontSize = Theme.of(context).textTheme.bodyText2.fontSize;
    double _buttonPadding = 15;

    return _buildCreatePlaylistButton(context, _buttonVerticalMargin, _buttonFontSize, _buttonPadding);
  }
}
