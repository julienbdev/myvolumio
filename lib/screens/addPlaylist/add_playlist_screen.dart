import 'package:flutter/material.dart';
import 'package:myvolumio/app_localizations.dart';
import 'package:myvolumio/screens/addPlaylist/new_playlist_button_widget.dart';
import 'package:myvolumio/screens/addPlaylist/playlists_widget.dart';

class AddPlaylistScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final dynamic _item = ModalRoute.of(context).settings.arguments;

    return Container(
      decoration: BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topLeft,
          end: Alignment.bottomRight,
          colors: [Colors.grey[800], Colors.black.withOpacity(0.6)],
        ),
      ),
      child: Scaffold(
        // By defaut, Scaffold background is white
        // Set its value to transparent
        backgroundColor: Colors.transparent,
        appBar: AppBar(
          backgroundColor: Colors.transparent,
          shadowColor: Colors.transparent,
          centerTitle: true,
          title: Text(AppLocalizations.of(context).translate('addToPlaylist')),
        ),
        body: Column(
          children: [
            NewPlaylistButtonWidget(_item),
            PlaylistsWidget(_item),
          ],
        ),
      ),
    );
  }
}
