import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:logging/logging.dart';
import 'package:myvolumio/app_localizations.dart';
import 'package:myvolumio/models/volumio_user.dart';
import 'package:myvolumio/services/socket_service.dart';
import 'package:myvolumio/services/stats_service.dart';
import 'package:myvolumio/services/user_service.dart';
import 'package:myvolumio/utilities/style_utility.dart';
import 'package:myvolumio/utilities/route_name_utility.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final locator = GetIt.instance;
  final _scaffoldKey = new GlobalKey<ScaffoldState>();
  bool _isLoading = false;
  bool _rememberMe = false;
  SharedPreferences prefs;
  final _auth = FirebaseAuth.instance;
  final _formKey = GlobalKey<FormState>();
  String _email;
  String _password;
  StatsService statsService;
  SocketService socketService;
  static final logger = Logger('LoginScreen');
  
  TextEditingController _emailController = TextEditingController();
  TextEditingController _passwordController = TextEditingController();

  @override
  void initState() {
    super.initState();
    statsService = locator.get<StatsService>();
    socketService = locator.get<SocketService>();
  }

  void _updatePrefs() async {
    if (prefs == null) {
      prefs = await SharedPreferences.getInstance();
    }
    prefs.setBool('rememberMe', _rememberMe);
  }

  String _validateEmail(String email) {
    if (email.isEmpty) {
      return AppLocalizations.of(context).translate('emailMandatory');
    }

    return null;
  }

  String _validatePassword(String password) {
    if (password.isEmpty) {
      return AppLocalizations.of(context).translate('passwordMandatory');
    }

    return null;
  }

  void _submitForm() {
    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();
      _signIn(_email, _password);
    }
  }

  VolumioUser _buildVolumioUser(DocumentSnapshot<Map<String, dynamic>> userDocument) {
    VolumioUser user = VolumioUser();
    user.name = userDocument.data()['name'];
    user.email = userDocument.data()['email'];
    user.id = userDocument.id;
    user.volumioHost = userDocument.data()['volumioHost'];
    user.volumioPort = userDocument.data()['volumioPort'];

    return user;
  }

  _signIn(String email, String password) async {
    setState(() {
      _isLoading = true;
    });
    try {
      UserCredential credential = await _auth.signInWithEmailAndPassword(email: email, password: password);
      DocumentSnapshot userDocument = await FirebaseFirestore.instance.collection('users').doc(credential.user.uid).get();
      // Build user for home screen
      VolumioUser user = _buildVolumioUser(userDocument);
      // save connected user in memory
      locator.get<UserService>().udpateUser(user);
      statsService.init(user.id, user.volumioHost, FirebaseFirestore.instance);
      socketService.initStreams();
      Navigator.of(context).pushNamedAndRemoveUntil(RouteNameUtility.home, ModalRoute.withName('/'));
    } on FirebaseAuthException catch (err) {
      setState(() {
        _isLoading = false;
      });
      logger.severe(err);
      _scaffoldKey.currentState.showSnackBar(
        SnackBar(
          content: Text(AppLocalizations.of(context).translate(err.code)),
        ),
      );
    } catch (err) {
      setState(() {
        _isLoading = false;
      });
      logger.severe(err);
      _scaffoldKey.currentState.showSnackBar(
        SnackBar(
          content: Text(AppLocalizations.of(context).translate('errorOccured')),
        ),
      );
    }
  }

  Widget _buildEmailInput() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          AppLocalizations.of(context).translate('email'),
        ),
        SizedBox(height: 10),
        TextFormField(
          keyboardType: TextInputType.emailAddress,
          autocorrect: false,
          textCapitalization: TextCapitalization.none,
          textAlignVertical: TextAlignVertical.center,
          controller: _emailController,
          validator: (value) => _validateEmail(value),
          onSaved: (val) => _email = val,
          decoration: InputDecoration(
            prefixIcon: Icon(
              Icons.email,
            ),
            hintText: AppLocalizations.of(context).translate('emailHint'),
          ),
        ),
      ],
    );
  }

  Widget _buildPasswordInput() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          AppLocalizations.of(context).translate('password'),
        ),
        SizedBox(height: 10),
        TextFormField(
          obscureText: true,
          textAlignVertical: TextAlignVertical.center,
          controller: _passwordController,
          validator: (value) => _validatePassword(value),
          onSaved: (val) => _password = val,
          decoration: InputDecoration(
            prefixIcon: Icon(
              Icons.lock,
            ),
            hintText: AppLocalizations.of(context).translate('passwordHint'),
          ),
        ),
      ],
    );
  }

  Widget _buildImageLogo() {
    return Image.asset(
      'assets/images/volumio-logo.png',
      fit: BoxFit.contain,
    );
  }

  Widget _buildForgotPasswordButton() {
    return Container(
      alignment: Alignment.centerRight,
      child: FlatButton(
        onPressed: () => logger.info('Forgot Password Button Pressed'),
        padding: EdgeInsets.only(right: 0.0),
        child: Text(
          AppLocalizations.of(context).translate('forgotPassword'),
        ),
      ),
    );
  }

  Widget _buildRememberMeCheckbox() {
    return Container(
      child: Row(
        children: [
          Checkbox(
            value: _rememberMe,
            onChanged: (value) {
              setState(() {
                _rememberMe = value;
              });
              _updatePrefs();
            },
          ),
          Text(
            AppLocalizations.of(context).translate('rememberMe'),
          ),
        ],
      ),
    );
  }

  Widget _buildLoginButton() {
    double padding = 15.0;
    double fontSize = Theme.of(context).textTheme.headline5.fontSize;
    double loadingSize = fontSize + (padding / 2);

    return Container(
      width: double.infinity,
      child: RaisedButton(
        elevation: 5.0,
        onPressed: () {
          _submitForm();
        },
        padding: EdgeInsets.all(padding),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(30.0),
        ),
        child: _isLoading
            ? SizedBox(
                height: loadingSize,
                width: loadingSize,
                child: CircularProgressIndicator(
                  valueColor: new AlwaysStoppedAnimation<Color>(
                    Colors.white,
                  ),
                ),
              )
            : Text(
                AppLocalizations.of(context).translate('login'),
                style: TextStyle(
                  fontSize: fontSize,
                  fontWeight: FontWeight.bold,
                ),
              ),
      ),
    );
  }

  Widget _buildSignupButton() {
    return Container(
      alignment: Alignment.bottomCenter,
      child: FlatButton(
        onPressed: () => Navigator.of(context).pushNamed(RouteNameUtility.signup),
        padding: EdgeInsets.only(right: 0.0),
        child: Text(
          AppLocalizations.of(context).translate('signup'),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final Map<String, String> args = ModalRoute.of(context).settings.arguments;
    if (args != null) {
      if (args['email'] != null) {
        _emailController.text = args['email'];
      }
      if (args['password'] != null) {
        _passwordController.text = args['password'];
      }
    }

    return Scaffold(
      key: _scaffoldKey,
      body: Stack(
        children: [
          StyleUtility.buildBackground(),
          Container(
            height: double.infinity,
            width: double.infinity,
            child: SingleChildScrollView(
              padding: EdgeInsets.fromLTRB(
                (MediaQuery.of(context).size.width * 7) / 100, // left
                (MediaQuery.of(context).size.height * 15) / 100, // top
                (MediaQuery.of(context).size.width * 7) / 100, // right
                (MediaQuery.of(context).size.height * 0) / 100, // bottom
              ),
              child: Form(
                key: _formKey,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    _buildImageLogo(),
                    SizedBox(height: 30),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        _buildEmailInput(),
                        SizedBox(height: 15),
                        _buildPasswordInput(),
                        _buildForgotPasswordButton(),
                        _buildRememberMeCheckbox(),
                        SizedBox(height: 30),
                        _buildLoginButton(),
                        SizedBox(height: 50),
                        _buildSignupButton(),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
