import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:myvolumio/services/socket_service.dart';

class PreviousButtonWidget extends StatelessWidget {
  final SocketService _socketService = GetIt.instance.get<SocketService>();
  final double iconSize;

  PreviousButtonWidget(this.iconSize);

  @override
  Widget build(BuildContext context) {
    return IconButton(
      splashColor: Colors.transparent,
      padding: EdgeInsets.zero,
      iconSize: iconSize,
      icon: Icon(
        Icons.skip_previous,
      ),
      onPressed: () {
        _socketService.emitPrev();
      },
    );
  }
}
