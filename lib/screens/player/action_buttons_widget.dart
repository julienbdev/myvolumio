import 'package:flutter/material.dart';
import 'package:myvolumio/screens/player/next_button_widget.dart';
import 'package:myvolumio/screens/player/play_pause_button_widget.dart';
import 'package:myvolumio/screens/player/previous_button_widget.dart';
import 'package:myvolumio/screens/player/random_button_widget.dart';
import 'package:myvolumio/screens/player/repeat_button_widget.dart';

class ActionButtonsWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double iconSize = IconTheme.of(context).size * 1.5;
    double bigIconSize = IconTheme.of(context).size * 2;

    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        RandomButtonWidget(iconSize),
        PreviousButtonWidget(iconSize),
        PlayPauseButtonWidget(bigIconSize),
        NextButtonWidget(iconSize),
        RepeatButtonWidget(iconSize),
      ],
    );
  }
}