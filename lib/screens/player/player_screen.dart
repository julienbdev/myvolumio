import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:logging/logging.dart';
import 'package:myvolumio/app_localizations.dart';
import 'package:myvolumio/screens/player/action_buttons_widget.dart';
import 'package:myvolumio/screens/player/album_art_widget.dart';
import 'package:myvolumio/screens/player/album_text_widget.dart';
import 'package:myvolumio/screens/player/song_info_widget.dart';
import 'package:myvolumio/services/player_service.dart';
import 'package:myvolumio/services/socket_service.dart';
import 'package:myvolumio/utilities/route_name_utility.dart';
import 'package:myvolumio/widgets/slider_widget.dart';

class PlayerScreen extends StatefulWidget {
  @override
  _PlayerScreenState createState() => _PlayerScreenState();
}

class _PlayerScreenState extends State<PlayerScreen> {
  final locator = GetIt.instance;
  SocketService socketService;
  PlayerService playerService;
  static final logger = Logger('PlayerScreen');

  @override
  initState() {
    super.initState();
    socketService = locator.get<SocketService>();
    playerService = locator.get<PlayerService>();

    playerService.toastStream.listen((event) {
      final snackBar = SnackBar(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0)),
        behavior: SnackBarBehavior.floating,
        content: Text(event.title),
        action: SnackBarAction(
          label: AppLocalizations.of(context).translate('close'),
          textColor: Colors.tealAccent,
          onPressed: () {
            ScaffoldMessenger.of(context).hideCurrentSnackBar();
          },
        ),
      );
      ScaffoldMessenger.of(context).showSnackBar(snackBar);
    });
  }

  Widget _buildAppBarTitle(BuildContext context) {
    return Text(
      AppLocalizations.of(context).translate('playingFromAlbum').toUpperCase(),
      style: TextStyle(
        fontSize: Theme.of(context).textTheme.subtitle2.fontSize,
      ),
    );
  }

  Widget _buildAppBarSubtitle() {
    return PreferredSize(
      child: AlbumTextWidget(),
      preferredSize: Size.fromHeight(Theme.of(context).textTheme.bodyText1.fontSize),
    );
  }

  Widget _buildPlayerBody(BuildContext context) {
    double imageContainerWidth = (MediaQuery.of(context).size.width * 85) / 100;
    double verticalPadding = (MediaQuery.of(context).size.height * 3) / 100;

    return Container(
      width: double.infinity,
      padding: EdgeInsets.fromLTRB(0, verticalPadding, 0, 0),
      child: Column(
        children: [
          AlbumArtWidget(imageContainerWidth),
          Container(
            width: imageContainerWidth,
            child: Column(
              children: [
                SongInfoWidget(),
                SliderWidget(),
                Padding(
                  padding: const EdgeInsets.only(top: 20),
                  child: ActionButtonsWidget(),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    // make sur keyboard is hidden when player is displayed
    FocusScope.of(context).unfocus();

    logger.info('player build');

    return Container(
      decoration: BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topLeft,
          end: Alignment.bottomRight,
          colors: [Colors.grey[800], Colors.black.withOpacity(0.6)],
        ),
      ),
      child: Scaffold(
        // By defaut, Scaffold background is white
        // Set its value to transparent
        backgroundColor: Colors.transparent,
        appBar: AppBar(
          backgroundColor: Colors.transparent,
          shadowColor: Colors.transparent,
          title: _buildAppBarTitle(context),
          centerTitle: true,
          bottom: _buildAppBarSubtitle(),
        ),
        body: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            _buildPlayerBody(
              context,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                IconButton(
                  icon: Icon(Icons.queue_music, color: Colors.white),
                  iconSize: IconTheme.of(context).size * 1.5,
                  onPressed: () {
                    Navigator.of(context).pushNamed(RouteNameUtility.queue);
                  },
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
