import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:myvolumio/models/volumio_user.dart';
import 'package:myvolumio/services/player_service.dart';
import 'package:myvolumio/services/user_service.dart';
import 'package:myvolumio/utilities/widget_utility.dart';

class AlbumArtWidget extends StatelessWidget {
  final PlayerService _playerService = GetIt.instance.get<PlayerService>();
  final VolumioUser _volumioUser = GetIt.instance.get<UserService>().getUser;
  final double imageContainerWidth;

  AlbumArtWidget(this.imageContainerWidth);

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<String>(
      stream: _playerService.albumArtStream,
      initialData: _playerService.lastAlbumArt,
      builder: (context, snapshot) {
        return CachedNetworkImage(
          imageUrl: WidgetUtility.buildAlbumArtUrl(snapshot.data, _volumioUser.volumioHost),
          imageBuilder: (context, imageProvider) => Container(
            width: imageContainerWidth,
            height: imageContainerWidth,
            decoration: BoxDecoration(
              image: DecorationImage(
                image: imageProvider,
                fit: BoxFit.cover,
              ),
            ),
          ),
          useOldImageOnUrlChange: true,
        );
      },
    );
  }
}
