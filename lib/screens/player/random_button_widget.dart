import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:myvolumio/services/player_service.dart';
import 'package:myvolumio/services/socket_service.dart';

class RandomButtonWidget extends StatelessWidget {
  final PlayerService _playerService = GetIt.instance.get<PlayerService>();
  final SocketService _socketService = GetIt.instance.get<SocketService>();
  final double iconSize;
  
  RandomButtonWidget(this.iconSize);

  @override
  Widget build(BuildContext context) {

    return StreamBuilder<bool>(
      stream: _playerService.randomStream,
      initialData: _playerService.lastRandom,
      builder: (context, snapshot) {
        return IconButton(
          padding: EdgeInsets.zero,
          iconSize: iconSize,
          color: snapshot.data ? Colors.tealAccent : Colors.white,
          icon: Icon(
            Icons.shuffle,
          ),
          onPressed: () {
            _socketService.emitRandom(!snapshot.data);
          },
        );
      },
    );
  }
}
