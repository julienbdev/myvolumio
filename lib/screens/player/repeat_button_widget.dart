import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:myvolumio/services/player_service.dart';
import 'package:myvolumio/services/socket_service.dart';
import 'package:myvolumio/utilities/pair.dart';

class RepeatButtonWidget extends StatelessWidget {
  final PlayerService _playerService = GetIt.instance.get<PlayerService>();
  final SocketService _socketService = GetIt.instance.get<SocketService>();
  final double iconSize;

  RepeatButtonWidget(this.iconSize);

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<Pair>(
      stream: _playerService.repeatStream,
      initialData: _playerService.lastRepeat,
      builder: (context, snapshot) {
        bool repeat = snapshot.data.left;
        bool repeatSingle = snapshot.data.right;
        
        return IconButton(
          padding: EdgeInsets.zero,
          color: (repeat || repeatSingle) ? Colors.tealAccent : Colors.white,
          iconSize: iconSize,
          icon: Icon(
            repeatSingle ? Icons.repeat_one : Icons.repeat,
          ),
          onPressed: () {
            bool repeatParam = false;
            bool repeatSingleParam = false;

            if (!repeat && !repeatSingle) {
              repeatParam = true;
              repeatSingleParam = false;
            }

            if (repeat && !repeatSingle) {
              repeatParam = true;
              repeatSingleParam = true;
            }

            if (repeat && repeatSingle) {
              repeatParam = false;
              repeatSingleParam = false;
            }

            _socketService.emitRepeat(repeatParam, repeatSingleParam);
          },
        );
      },
    );
  }
}
