import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:myvolumio/services/player_service.dart';
import 'package:myvolumio/services/socket_service.dart';

class PlayPauseButtonWidget extends StatelessWidget {
  final PlayerService _playerService = GetIt.instance.get<PlayerService>();
  final SocketService _socketService = GetIt.instance.get<SocketService>();
  final double iconSize;

  PlayPauseButtonWidget(this.iconSize);

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<String>(
      stream: _playerService.statustStream,
      initialData: _playerService.lastStatus,
      builder: (context, snapshot) {
        String status = snapshot.data;
        if (status.isNotEmpty) {
          if (status != 'play') {
            return IconButton(
              padding: EdgeInsets.zero,
              iconSize: iconSize,
              icon: Icon(Icons.play_circle_outline),
              onPressed: () {
                _socketService.emitPlay();
              },
            );
          }
          if (status == 'play') {
            return IconButton(
              padding: EdgeInsets.zero,
              iconSize: iconSize,
              icon: Icon(Icons.pause_circle_outline),
              onPressed: () {
                _socketService.emitPause();
              },
            );
          }
        }

        return IconButton(
          padding: EdgeInsets.zero,
          iconSize: iconSize,
          icon: Icon(Icons.play_arrow),
          onPressed: null,
        );
      },
    );
  }
}
