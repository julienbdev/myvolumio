import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:myvolumio/screens/player/artist_text_widget.dart';
import 'package:myvolumio/screens/player/song_text_widget.dart';
import 'package:myvolumio/services/player_service.dart';
import 'package:myvolumio/utilities/route_name_utility.dart';

class SongInfoWidget extends StatelessWidget {
  final PlayerService _playserService = GetIt.instance.get<PlayerService>();

  @override
  Widget build(BuildContext context) {
    return ListTile(
      contentPadding: EdgeInsets.only(top: 20),
      title: SongTextWidget(),
      subtitle: ArtistTextWidget(),
      trailing: IconButton(
        icon: Icon(Icons.add, size: IconTheme.of(context).size * 1.5),
        onPressed: () {
          Navigator.of(context).pushNamed(RouteNameUtility.addPlaylist, arguments: {
            'service': _playserService.lastService,
            'uri': _playserService.lastUri,
          });
        },
      ),
    );
  }
}
