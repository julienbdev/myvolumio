import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:logging/logging.dart';
import 'package:myvolumio/services/socket_service.dart';

class NextButtonWidget extends StatelessWidget {
  final SocketService _socketService = GetIt.instance.get<SocketService>();
  final double iconSize;

  NextButtonWidget(this.iconSize);

  @override
  Widget build(BuildContext context) {
    return IconButton(
      splashColor: Colors.transparent,
      padding: EdgeInsets.zero,
      iconSize: iconSize,
      icon: Icon(
        Icons.skip_next,
      ),
      onPressed: () {
        _socketService.emitNext();
      },
    );
  }
}
