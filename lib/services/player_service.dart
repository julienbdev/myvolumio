import 'dart:async';
import 'package:logging/logging.dart';
import 'package:myvolumio/models/toast.dart';
import 'package:myvolumio/utilities/pair.dart';

class PlayerService {
  final _positionStreamController = StreamController<int>.broadcast();
  final _lengthStreamController = StreamController<int>.broadcast();
  final _albumArtStreamController = StreamController<String>.broadcast();
  final _albumStreamController = StreamController<String>.broadcast();
  final _songStreamController = StreamController<String>.broadcast();
  final _artistStreamController = StreamController<String>.broadcast();
  final _randomStreamController = StreamController<bool>.broadcast();
  final _statusStreamController = StreamController<String>.broadcast();
  final _repeatStreamController = StreamController<Pair>.broadcast();
  final _uriStreamController = StreamController<String>.broadcast();
  final _toastStreamController = StreamController<Toast>.broadcast();

  String lastAlbumArt = '';
  String lastAlbum = '';
  String lastSong = '';
  String lastArtist = '';
  String lastStatus = '';
  String lastUri = '';
  String lastService = '';
  bool lastRandom = false;
  Pair lastRepeat = Pair(false, false);

  Timer timer;
  int length;
  int position;
  static final logger = Logger('PlayerService');

  Stream<int> get positionStream {
    return _positionStreamController.stream;
  }

  Stream<int> get lengthStream {
    return _lengthStreamController.stream;
  }

  Stream<String> get albumArtStream {
    return _albumArtStreamController.stream;
  }

  Stream<String> get albumStream {
    return _albumStreamController.stream;
  }

  Stream<String> get songStream {
    return _songStreamController.stream;
  }

  Stream<String> get artistStream {
    return _artistStreamController.stream;
  }

  Stream<bool> get randomStream {
    return _randomStreamController.stream;
  }

  Stream<String> get statustStream {
    return _statusStreamController.stream;
  }

  Stream<Pair> get repeatStream {
    return _repeatStreamController.stream;
  }

  Stream<String> get uriStream {
    return _uriStreamController.stream;
  }

  Stream<Toast> get toastStream {
    return _toastStreamController.stream;
  }

  void updateAlbumArt(String albumArt) {
    _albumArtStreamController.sink.add(albumArt);
  }

  void updateAlbum(String album) {
    _albumStreamController.sink.add(album);
  }

  void updateSong(String song) {
    _songStreamController.sink.add(song);
  }

  void updateArtist(String artist) {
    _artistStreamController.sink.add(artist);
  }

  void updateRandom(bool random) {
    _randomStreamController.sink.add(random);
  }

  void updateStatus(String status) {
    _statusStreamController.sink.add(status);
  }

  void updateRepeat(Pair repeat) {
    _repeatStreamController.sink.add(repeat);
  }

  void updateUri(String uri) {
    _uriStreamController.sink.add(uri);
  }

  void updateToast(String type, String title, String message) {
    _toastStreamController.sink.add(new Toast(type, title, message));
  }

  void setLength(int songLength) {
    length = songLength;
    _lengthStreamController.sink.add(length);
  }

  void setPosition(int songPosition) {
    position = songPosition;
    _positionStreamController.sink.add(songPosition);
  }

  void play() {
    logger.info('player service play');
    if (timer == null || !timer.isActive) {
      timer = Timer.periodic(Duration(seconds: 1), (timer) {
        if (position >= length) {
          position = 0;
        }
        position += 1;
        _positionStreamController.sink.add(position);
      });
    }
  }

  void stop() {
    position = 0;
    if (timer != null) {
      timer.cancel();
    }
  }

  void pause() {
    if (timer != null) {
      timer.cancel();
    }
  }

  void dispose() {
    _positionStreamController.close();
    _lengthStreamController.close();
    _albumArtStreamController.close();
    _albumStreamController.close();
    _songStreamController.close();
    _artistStreamController.close();
    _randomStreamController.close();
    _statusStreamController.close();
    _repeatStreamController.close();
    _uriStreamController.close();
    _toastStreamController.close();
  }
}
