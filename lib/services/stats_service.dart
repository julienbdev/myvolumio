import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:logging/logging.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:myvolumio/models/last_played_item.dart';
import 'package:myvolumio/utilities/item_type_num.dart';

class StatsService {
  FirebaseFirestore _db;
  String _userId;
  String _volumioHost;
  static String _lastPlayedItemsCollection = 'lastPlayedItems';
  static String _favoriteAlbumsCollection = 'favoriteAlbums';
  static String _favoriteArtistsCollection = 'favoriteArtists';
  static String _favoritePlaylistsCollection = 'favoritePlaylists';
  static int _maxLastPlayedItems = 12;
  static int _maxFavoriteAlbums = 10;
  static int _maxFavoriteArtists = 10;
  static final logger = Logger('StatsService');

  List<LastPlayedItem> cachedLastPlayedItems;
  List<LastPlayedItem> cachedFavoriteAlbums;
  List<LastPlayedItem> cachedFavoriteArtists;
  List<LastPlayedItem> cachedWelcomeTiles;

  void init(String userId, String volumioHost, FirebaseFirestore db) {
    _userId = userId;
    _db = db;
    _volumioHost = volumioHost;
  }

  Future<void> initStats() async {
    await _db.collection(_lastPlayedItemsCollection).doc(_userId).set({'itemsCount': 0});
  }

  Future<List<LastPlayedItem>> getLastPlayedItems() async {
    DocumentReference docRef = _db.collection(_lastPlayedItemsCollection).doc(_userId);
    QuerySnapshot query = await docRef.collection('items').orderBy('created', descending: true).get();
    cachedLastPlayedItems = query.docs.map((e) => LastPlayedItem.fromJson(e.data())).toList();
    return cachedLastPlayedItems;
  }

  Future<List<LastPlayedItem>> getFavoriteAlbums() async {
    DocumentReference docRef = _db.collection(_favoriteAlbumsCollection).doc(_userId);
    Timestamp mindate = Timestamp.fromDate(DateTime.now().subtract(new Duration(days: 30)));
    QuerySnapshot query =
        await docRef.collection('items').where('created', isGreaterThanOrEqualTo: mindate).orderBy('created', descending: true).orderBy('count', descending: true).limit(_maxFavoriteAlbums).get();

    cachedFavoriteAlbums = query.docs.map((e) => LastPlayedItem.fromJson(e.data())).toList();
    return cachedFavoriteAlbums;
  }

  Future<List<LastPlayedItem>> getFavoriteArtists() async {
    DocumentReference docRef = _db.collection(_favoriteArtistsCollection).doc(_userId);
    Timestamp mindate = Timestamp.fromDate(DateTime.now().subtract(new Duration(days: 30)));
    QuerySnapshot query =
        await docRef.collection('items').where('created', isGreaterThanOrEqualTo: mindate).orderBy('created', descending: true).orderBy('count', descending: true).limit(_maxFavoriteArtists).get();

    cachedFavoriteArtists =  query.docs.map((e) => LastPlayedItem.fromJson(e.data())).toList();
    return cachedFavoriteArtists;
  }

  Future<List<LastPlayedItem>> getWelcomeTiles() async {
    List<LastPlayedItem> tiles = [];

    // 2 last fav artists
    // 2 last fav albums
    // 2 last fav playlists
    Timestamp mindate = Timestamp.fromDate(DateTime.now().subtract(new Duration(days: 30)));

    DocumentReference docRefArtists = _db.collection(_favoriteArtistsCollection).doc(_userId);
    QuerySnapshot queryArtists =
        await docRefArtists.collection('items').where('created', isGreaterThanOrEqualTo: mindate).orderBy('created', descending: true).orderBy('count', descending: true).limit(2).get();
    tiles.addAll(queryArtists.docs.map((e) => LastPlayedItem.fromJson(e.data())).toList());

    DocumentReference docRefAlbums = _db.collection(_favoriteAlbumsCollection).doc(_userId);
    QuerySnapshot queryAlbums =
        await docRefAlbums.collection('items').where('created', isGreaterThanOrEqualTo: mindate).orderBy('created', descending: true).orderBy('count', descending: true).limit(2).get();
    tiles.addAll(queryAlbums.docs.map((e) => LastPlayedItem.fromJson(e.data())).toList());

    DocumentReference docRefPlaylists = _db.collection(_favoritePlaylistsCollection).doc(_userId);
    QuerySnapshot queryPlaylists =
        await docRefPlaylists.collection('items').where('created', isGreaterThanOrEqualTo: mindate).orderBy('created', descending: true).orderBy('count', descending: true).limit(2).get();
    tiles.addAll(queryPlaylists.docs.map((e) => LastPlayedItem.fromJson(e.data())).toList());

    cachedWelcomeTiles = tiles;
    return cachedWelcomeTiles;
  }

  addLastPlayedItemPlaylist(String playlistName, List<String> albumArts) async {
    LastPlayedItem item = LastPlayedItem();
    item.title = playlistName;
    item.albumArts = albumArts;
    item.type = ItemTypeEnum.playlist;
    item.uri = playlistName;

    logger.info('LastPLayedItem - playlist : ${item.toString()}');

    return addLastPlayedItem(item);
  }

  addLastPlayedItemAlbum(String album, String uri, String albumArt) async {
    LastPlayedItem item = LastPlayedItem();
    item.title = album;
    item.type = ItemTypeEnum.album;
    item.uri = uri;
    item.albumArt = albumArt;

    logger.info('LastPLayedItem - album : ${item.toString()}');

    return addLastPlayedItem(item);
  }

  addLastPlayedItemArtist(String artist, String uri, String albumArt) async {
    LastPlayedItem item = LastPlayedItem();
    item.title = artist;
    item.type = ItemTypeEnum.artist;
    item.uri = uri;
    item.albumArt = albumArt;

    logger.info('LastPLayedItem - album : ${item.toString()}');

    return addLastPlayedItem(item);
  }

  addLastPlayedItem(LastPlayedItem item) async {
    item.created = Timestamp.now();

    DocumentReference docRef = _db.collection(_lastPlayedItemsCollection).doc(_userId);
    DocumentSnapshot lastPlayedDoc = await docRef.get();
    int itemCount = lastPlayedDoc.get('itemsCount');
    logger.info('itemCount $itemCount');

    String uri = item.uri;
    // check if item is already in the list
    QuerySnapshot queryExists = await docRef.collection('items').where('uri', isEqualTo: uri).get();
    if (queryExists.size == 0) {
      if (itemCount < _maxLastPlayedItems) {
        await docRef.collection('items').add(item.toJson());
        await docRef.update({'itemsCount': FieldValue.increment(1)});
      } else {
        // remove older item
        QuerySnapshot query = await docRef.collection('items').orderBy('created').limit(1).get();
        String idRemoveDoc = query.docs.first.id;
        logger.info('id to remove : $idRemoveDoc');
        docRef.collection('items').doc(idRemoveDoc).delete();
        // add new item
        await docRef.collection('items').add(item.toJson());
      }
    } else {
      // update timestamp
      docRef.collection('items').doc(queryExists.docs[0].id).update({
        'created': Timestamp.now(),
      });
    }
  }

  addFavoriteAlbum(LastPlayedItem item) async {
    DocumentReference docRef = _db.collection(_favoriteAlbumsCollection).doc(_userId);
    QuerySnapshot queryExists = await docRef.collection('items').where('title', isEqualTo: item.title).get();
    if (queryExists.size == 0) {
      item.created = Timestamp.now();
      item.count = 1;
      await docRef.collection('items').doc().set(item.toJson());
    } else {
      logger.info('update favorite album');
      docRef.collection('items').doc(queryExists.docs[0].id).update({
        'count': FieldValue.increment(1),
        'created': Timestamp.now(),
      });
    }
  }

  addFavoriteArtist(LastPlayedItem item) async {
    DocumentReference docRef = _db.collection(_favoriteArtistsCollection).doc(_userId);
    QuerySnapshot queryExists = await docRef.collection('items').where('title', isEqualTo: item.title).get();
    if (queryExists.size == 0) {
      bool albumArtMissing = item.albumArt == null || item.albumArt.isEmpty;
      bool uriMissing = item.uri == null || item.uri.isEmpty;
      if (albumArtMissing || uriMissing) {
        String url = 'http://$_volumioHost/api/v1/browse?uri=artists://${Uri.encodeFull(item.title)}';
        logger.info('artist recovery url : $url');
        http.Response response = await http.get(Uri.parse(url));

        if (response.body != null) {
          logger.info(response.body);
          Map data = json.decode(response.body);
          if (data['navigation'] != null && data['navigation']['info'] != null) {
            if (uriMissing) {
              item.uri = data['navigation']['info']['uri'];
            }
            if (albumArtMissing) {
              item.albumArt = data['navigation']['info']['albumart'];
            }
          } else if (albumArtMissing) {
            // add album art placeholder with volumio tinyart
            // exemple : /tinyart/led_zeppelin/large
            String artist = item.title.toLowerCase().replaceAll(' ', '_').replaceAll('/', '_');
            item.albumArt = '/tinyart/$artist/large';
          }
        } else if (albumArtMissing) {
          // add album art placeholder with volumio tinyart
          // exemple : /tinyart/led_zeppelin/large
          String artist = item.title.toLowerCase().replaceAll(' ', '_').replaceAll('/', '_');
          item.albumArt = '/tinyart/$artist/large';
        }
      }
      item.created = Timestamp.now();
      item.count = 1;
      await docRef.collection('items').doc().set(item.toJson());
    } else {
      logger.info('update favorite artist');
      docRef.collection('items').doc(queryExists.docs[0].id).update({
        'count': FieldValue.increment(1),
        'created': Timestamp.now(),
        if (item.albumArt != null && item.albumArt.isNotEmpty) 'albumArt': item.albumArt,
      });
    }
  }

  addFavoritePlaylist(String playlistName, List<String> albumArts) async {
    LastPlayedItem item = LastPlayedItem();
    item.title = playlistName;
    item.albumArts = albumArts;
    item.type = ItemTypeEnum.playlist;
    item.uri = playlistName;
    item.created = Timestamp.now();

    DocumentReference docRef = _db.collection(_favoritePlaylistsCollection).doc(_userId);
    QuerySnapshot queryExists = await docRef.collection('items').where('title', isEqualTo: item.title).get();

    if (queryExists.size == 0) {
      item.count = 1;
      await docRef.collection('items').doc().set(item.toJson());
    } else {
      docRef.collection('items').doc(queryExists.docs[0].id).update({
        'count': FieldValue.increment(1),
        'created': Timestamp.now(),
        'albumArts': albumArts,
      });
    }
  }
}
