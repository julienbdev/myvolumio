import 'package:myvolumio/models/volumio_user.dart';

class UserService {
  VolumioUser _user;

  VolumioUser get getUser {
    return _user;
  }

  udpateUser(upatedUser) {
    _user = upatedUser;
  }

  updateHost(String host) {
    _user.volumioHost = host;
  }

  updatePort(int port) {
    _user.volumioPort = port;
  }
}