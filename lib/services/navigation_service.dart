import 'package:flutter/material.dart';

const HOME_PAGE = 0;
const SEARCH_PAGE = 1;
const LIBRARY_PAGE = 2;

class NavigationService {
  // playlist to display
  String playlistName = '';
  // artist uri to display
  String artistUri = '';
  // album uri to display
  String albumUri = '';
  // arsist art
  String artistArt = '';
  // album goto
  String albumGoto;
  // artist goto
  String artistGoto;
  // navigation history used by home screen back button
  List<int> history = [0];
  // Current bottom bar page
  int currentPage = 0;
  // Nested navigation key
  GlobalKey<NavigatorState> navigatorKey = GlobalKey();

  void updateNavigationInfo(String navAlbumUri, String navArtistUri, String artistAlbumArt, String album, String artist) {
    albumUri = navAlbumUri;
    artistUri = navArtistUri;
    albumGoto = album;
    artistGoto = artist;
    artistArt = artistAlbumArt;

    if ((navAlbumUri != null && navAlbumUri.isNotEmpty) || (navArtistUri != null && navArtistUri.isNotEmpty)) {
      // uri and goto fields are exclusif, can't be both
      albumGoto = null;
      artistGoto = null;
    }
  }

  void backIndex() {
    if (history.isNotEmpty) {
      history.removeLast();
    }
    if (history.isEmpty) {
      history.add(0);
    }
    currentPage = history.last;
  }
}
