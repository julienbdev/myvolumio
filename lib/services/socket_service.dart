import 'dart:async';
import 'package:get_it/get_it.dart';
import 'package:logging/logging.dart';
import 'package:myvolumio/models/volumio_goto.dart';
import 'package:myvolumio/plugins/notification_plugin.dart';
import 'package:myvolumio/services/player_service.dart';
import 'package:myvolumio/utilities/pair.dart';
import 'package:socket_io_client/socket_io_client.dart' as IO;

class SocketService {
  IO.Socket socket;
  StreamController<dynamic> _playerStateStream = StreamController<dynamic>.broadcast();
  StreamController<dynamic> _connectedStream = StreamController<bool>.broadcast();
  StreamController<dynamic> _playlistsStream = StreamController<dynamic>.broadcast();
  StreamController<dynamic> _playlistContentStream = StreamController<dynamic>.broadcast();
  StreamController<dynamic> _browseLibraryStream = StreamController<dynamic>.broadcast();
  StreamController<dynamic> _searchLibraryStream = StreamController<dynamic>.broadcast();
  StreamController<dynamic> _artistStream = StreamController<dynamic>.broadcast();
  StreamController<dynamic> _albumStream = StreamController<dynamic>.broadcast();
  StreamController<dynamic> _queueStream = StreamController<dynamic>.broadcast();

  final _playerService = GetIt.instance.get<PlayerService>();

  static final logger = Logger('SocketService');

  dynamic _lastItem = {};
  dynamic cachedArtists;
  dynamic cachedAlbums;
  dynamic cachedSearch;

  void _updateBrowseLibrary(dynamic data) {
    // artist, album or search share same event
    // => analyse data to dispatch to the correct stream
    if (data['navigation']['isSearchResult'] != null && data['navigation']['isSearchResult'] == true) {
      _searchLibraryStream.sink.add(data);
    } else if (data['navigation']['info'] != null) {
      if (data['navigation']['info']['type'] == 'artist') {
        _artistStream.sink.add(data);
      }
      if (data['navigation']['info']['type'] == 'album') {
        _albumStream.sink.add(data);
      }
    } else {
      _browseLibraryStream.sink.add(data);
    }
  }

  void _updatePlaylistContent(dynamic data) {
    _playlistContentStream.sink.add(data);
  }

  void _updatePlaylists(dynamic data) {
    _playlistsStream.sink.add(data);
  }

  void _updatePlayerState(dynamic data) {
    _playerStateStream.sink.add(data);
  }

  void _updateQueue(dynamic data) {
    _queueStream.sink.add(data);
  }

  void emitPlaylistContent(String name) {
    socket.emit('getPlaylistContent', {'name': name});
  }

  void emitPlayerState() {
    socket.emit('getState');
  }

  void emitPlay([int index]) {
    if (index != null) {
      socket.emit('play', {'value': index});
    } else {
      socket.emit('play');
    }
  }

  void emitPause() {
    socket.emit('pause');
  }

  void emitPrev() {
    socket.emit('prev');
  }

  void emitNext() {
    socket.emit('next');
  }

  void emitRandom(bool random) {
    socket.emit('setRandom', {'value': random});
  }

  void emitRepeat(bool repeat, bool repeatSingle) {
    socket.emit('setRepeat', {'value': repeat, 'repeatSingle': repeatSingle});
  }

  void emitSeek(int sec) {
    socket.emit('seek', sec);
  }

  void emitPlaylists() {
    socket.emit('listPlaylist');
  }

  void emitPlayPlaylist(String name, [bool random = false]) {
    socket.emit('setRandom', {'value': random});
    socket.emit('playPlaylist', {'name': name});
  }

  void emitPlayItemsList(item, [list, index, bool random = false]) {
    if (random) {
      socket.emit('setRandom', {'value': random});
    }

    socket.emit('playItemsList', {'item': item, if (list != null) 'list': list, if (index != null) 'index': index});
  }

  void emitBrowseLibraryArtists() {
    socket.emit('browseLibrary', {'uri': 'artists://'});
  }

  void emitBrowseLibraryAlbums() {
    socket.emit('browseLibrary', {'uri': 'albums://'});
  }

  void emitBrowseLibrary(String uri) {
    socket.emit('browseLibrary', {'uri': uri});
  }

  void emitGoTo(VolumioGoTo goto) {
    socket.emit(
      'goTo',
      {
        'value': goto.value,
        if (goto.album != null) 'album': goto.album,
        if (goto.artist != null) 'artist': goto.artist,
        'type': goto.type,
      },
    );
  }

  void emitSearchLibrary(String query) {
    socket.emit('search', {'value': query, 'type': 'any', 'plugin_name': 'mpd'});
  }

  void clearSearchLibrary() {
    _searchLibraryStream.sink.add(null);
  }

  void emitGetQueue() {
    socket.emit('getQueue');
  }

  void emitRemoveFromQueue(int index) {
    socket.emit('removeFromQueue', {'value': index});
  }

  void emitClearQueue() {
    socket.emit('clearQueue');
  }

  void emitAddToPlaylist(String playlistName, String itemService, String itemUri) {
    socket.emit('addToPlaylist', {
      'name': playlistName,
      'service': itemService,
      'uri': itemUri,
    });
  }

  Stream<dynamic> getPlaylistContentStream() {
    return _playlistContentStream.stream;
  }

  Stream<dynamic> getPlaylistsStream() {
    return _playlistsStream.stream;
  }

  Stream<dynamic> getPlayserStateStream() {
    return _playerStateStream.stream;
  }

  Stream<bool> getConnectedStream() {
    return _connectedStream.stream;
  }

  Stream<dynamic> getBrowseLibraryStream() {
    return _browseLibraryStream.stream;
  }

  Stream<dynamic> getSearchLibraryStream() {
    return _searchLibraryStream.stream;
  }

  Stream<dynamic> getArtistLibraryStream() {
    return _artistStream.stream;
  }

  Stream<dynamic> getAlbumLibraryStream() {
    return _albumStream.stream;
  }

  Stream<dynamic> getQueueStream() {
    return _queueStream.stream;
  }

  void initStreams() {
    _playerStateStream = StreamController<dynamic>.broadcast();
    _connectedStream = StreamController<bool>.broadcast();
    _playlistsStream = StreamController<dynamic>.broadcast();
    _playlistContentStream = StreamController<dynamic>.broadcast();
    _browseLibraryStream = StreamController<dynamic>.broadcast();
    _searchLibraryStream = StreamController<dynamic>.broadcast();
    _artistStream = StreamController<dynamic>.broadcast();
    _albumStream = StreamController<dynamic>.broadcast();
    _queueStream = StreamController<dynamic>.broadcast();
  }

  IO.Socket connect(String url) {
    socket = IO.io(
      url,
      IO.OptionBuilder().setTransports(['websocket']).enableAutoConnect().build(),
    );

    socket.onConnect((_) {
      logger.info('socket connected');
      // Home screen is listening to _connectedStream
      // to display correct home page when the user is connected
      _connectedStream.sink.add(true);
      setup();
    });

    socket.onConnectError((data) => logger.info('socket onConnectError'));
    socket.onConnectTimeout((data) => logger.info('socket onConnectTimeout'));
    socket.onError((data) => logger.info('socket onError'));
    socket.onDisconnect((data) => logger.info('socket onDisconnect'));
    socket.onReconnect((data) => logger.info('socket onReconnect'));
    socket.onReconnectAttempt((data) => logger.info('socket onReconnectAttempt'));

    socket.connect();

    return socket;
  }

  void disconnect() {
    socket.dispose();
    socket = null;
    dispose();
  }

  void setup() {
    socket.on('pushState', (data) {
      logger.config('pushState');
      // push new data to the stream
      _updatePlayerState(data);

      // update current song length (in second)
      _playerService.setLength(data['duration']);
      // compute seeking position (convert from millisecond to second)
      int position = ((data['seek'] as int) / 1000).round();
      _playerService.setPosition(position);
      // update player status
      if (data['status'] == 'pause') {
        _playerService.pause();
      }
      if (data['status'] == 'play') {
        _playerService.play();
      }
      if (data['status'] == 'stop') {
        _playerService.stop();
      }

      if (data['albumart'] != _playerService.lastAlbumArt) {
        _playerService.lastAlbumArt = data['albumart'];
        _playerService.updateAlbumArt(data['albumart']);
      }
      if (data['album'] != _playerService.lastAlbum) {
        _playerService.lastAlbum = data['album'];
        _playerService.updateAlbum(data['album']);
      }
      if (data['title'] != _playerService.lastSong) {
        _playerService.lastSong = data['title'];
        _playerService.updateSong(data['title']);
      }
      if (data['artist'] != _playerService.lastArtist) {
        _playerService.lastArtist = data['artist'];
        _playerService.updateArtist(data['artist']);
      }
      if (data['random'] != _playerService.lastRandom) {
        _playerService.lastRandom = data['random'];
        _playerService.updateRandom(data['random']);
      }
      if (data['status'] != _playerService.lastStatus) {
        _playerService.lastStatus = data['status'];
        _playerService.updateStatus(data['status']);
      }
      if (data['repeat'] != _playerService.lastRepeat.left || data['repeatSingle'] != _playerService.lastRepeat.right) {
        _playerService.lastRepeat = Pair(data['repeat'] as bool, data['repeatSingle'] as bool);
        _playerService.updateRepeat(_playerService.lastRepeat);
      }
      if (data['uri'] != _playerService.lastUri) {
        _playerService.lastUri = data['uri'];
        _playerService.updateUri(data['uri']);
      }

      if (data['service'] != _playerService.lastService) {
        _playerService.lastService = data['service'];
      }

      if (data['uri'] != _lastItem['uri'] || data['status'] != _lastItem['status']) {
        logger.info('new song played or status changed');
        NotificationPlugin.showNotification(
          song: data['title'],
          artist: data['artist'],
          isPlaying: data['status'] == 'play',
          albumArt: data['albumart'],
        );
      }
      _lastItem = data;
    });

    socket.on('pushListPlaylist', (data) {
      _updatePlaylists(data);
    });

    socket.on('pushPlaylistContent', (data) {
      _updatePlaylistContent(data);
    });

    socket.on('pushBrowseLibrary', (data) {
      _updateBrowseLibrary(data);
    });

    socket.on('pushQueue', (data) {
      _updateQueue(data);
    });

    /* NOT WORKING, MISSING DATA
    socket.on('pushAddToPlaylist', (data) {
      logger.info('pushAddToPlaylist');
      logger.info(data); // data is always empty, not working !
    });
    */

    socket.on('pushToastMessage', (data) {
      logger.info(data);
      _playerService.updateToast(data['type'], data['title'], data['message']);
    });
  }

  bool isConnected() {
    return socket != null && socket.connected;
  }

  // TODO call dispose when the app is closed
  void dispose() {
    _playerStateStream.close();
    _connectedStream.close();
    _playlistsStream.close();
    _playlistContentStream.close();
    _browseLibraryStream.close();
    _searchLibraryStream.close();
    _artistStream.close();
    _albumStream.close();
    _queueStream.close();
  }
}
