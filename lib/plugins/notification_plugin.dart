import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';
import 'package:get_it/get_it.dart';
import 'package:logging/logging.dart';
import 'package:myvolumio/services/socket_service.dart';
import 'package:myvolumio/utilities/widget_utility.dart';

class NotificationPlugin {
  static final logger = Logger('NotificationPlugin');
  static final _socketService = GetIt.instance.get<SocketService>();
  static const MethodChannel _channel = const MethodChannel('notificationPlugin');
  static String _volumioHost;

  static Future<String> get platformVersion async {
    final String version = await _channel.invokeMethod('getPlatformVersion');
    return version;
  }

  static Future<void> showNotification({
    @required String song,
    @required String artist,
    @required bool isPlaying,
    @required String albumArt,
  }) async {
    _channel.invokeMethod(
      'showNotification',
      {
        'song': song,
        'artist': artist,
        'isPlaying': isPlaying,
        'albumArt': WidgetUtility.buildAlbumArtUrl(albumArt, _volumioHost),
      },
    );
  }

  static init(String volumioHost) {
    _volumioHost = volumioHost;
    _channel.setMethodCallHandler((call) {
      logger.info('call.method : ${call.method}');
      switch (call.method) {
        case "prev":
          _socketService.emitPrev();
          break;
        case "next":
          _socketService.emitNext();
          break;
        case "play":
          _socketService.emitPlay();
          break;
        case "pause":
          _socketService.emitPause();
          break;
      }
      return null;
    });
  }
}
