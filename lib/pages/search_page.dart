import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:logging/logging.dart';
import 'package:myvolumio/app_localizations.dart';
import 'package:myvolumio/models/volumio_user.dart';
import 'package:myvolumio/services/navigation_service.dart';
import 'package:myvolumio/services/socket_service.dart';
import 'package:myvolumio/services/user_service.dart';
import 'package:myvolumio/utilities/item_type_num.dart';
import 'package:myvolumio/utilities/route_name_utility.dart';
import 'package:myvolumio/utilities/widget_utility.dart';
import 'package:myvolumio/widgets/album_widget.dart';
import 'package:myvolumio/widgets/artist_widget.dart';

class SearchPage extends StatelessWidget {
  final NavigationService navigationService = GetIt.instance.get<NavigationService>();
  final SocketService socketService = GetIt.instance.get<SocketService>();
  static final logger = Logger('SearchPage');
  final GlobalKey<NavigatorState> navigatorKey = GlobalKey();
  final VolumioUser _connectedUser = GetIt.instance.get<UserService>().getUser;

  Widget _buildSearchPage(BuildContext context) {
    double horizontalPadding = (MediaQuery.of(context).size.width * 5) / 100;
    double verticalPadding = (MediaQuery.of(context).size.height * 2) / 100;

    return Container(
      height: double.infinity,
      width: double.infinity,
      decoration: BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          colors: [Color(0xFF1F2B2F).withOpacity(0.2), Colors.black.withOpacity(0.6)],
          stops: [0.0, 0.4],
          tileMode: TileMode.repeated,
        ),
      ),
      child: SafeArea(
        child: Padding(
          padding: EdgeInsets.fromLTRB(
            horizontalPadding, // left
            verticalPadding, // top
            horizontalPadding, // right
            0, // bottom
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                AppLocalizations.of(context).translate('search'),
                style: TextStyle(
                  fontSize: Theme.of(context).textTheme.headline5.fontSize,
                  fontWeight: FontWeight.bold,
                ),
              ),
              SizedBox(height: 20),
              TextField(
                keyboardType: TextInputType.text,
                autocorrect: false,
                textCapitalization: TextCapitalization.none,
                textAlignVertical: TextAlignVertical.center,
                decoration: InputDecoration(
                  prefixIcon: Icon(
                    Icons.search,
                  ),
                  hintText: AppLocalizations.of(context).translate('searchHint'),
                ),
                onChanged: (value) {
                  logger.info('input search : ' + value);
                  String search = value.trim();
                  if (search.isNotEmpty && search.length > 3) {
                    socketService.emitSearchLibrary(search);
                  }
                  if (search.isEmpty) {
                    socketService.clearSearchLibrary();
                    socketService.cachedSearch = null;
                  }
                },
              ),
              SizedBox(height: 20),
              _buildSearchResult(),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildSearchResult() {
    return Expanded(
      child: StreamBuilder<dynamic>(
        stream: socketService.getSearchLibraryStream(),
        initialData: socketService.cachedSearch,
        builder: (context, snapshot) {
          List<dynamic> results = [];
          if (snapshot.hasData) {
            List<dynamic> lists = snapshot.data['navigation']['lists'];
            lists.forEach((element) {
              results.addAll(element['items']);
            });
            socketService.cachedSearch = snapshot.data;
          }

          return ListView.builder(
            itemCount: results.length,
            itemBuilder: (context, index) {
              ItemTypeEnum itemType;
              dynamic data = results[index];

              if (data['type'] == 'song') {
                itemType = ItemTypeEnum.song;
              } else if ((data['uri'] as String).startsWith('artists')) {
                itemType = ItemTypeEnum.artist;
              } else if ((data['uri'] as String).startsWith('albums')) {
                itemType = ItemTypeEnum.album;
              }

              return ListTile(
                contentPadding: EdgeInsets.zero,
                leading: SizedBox(
                  width: (MediaQuery.of(context).size.width * 15) / 100,
                  child: _buildAlbumArt(itemType, data),
                ),
                title: Text(
                  '${data['title']}',
                  overflow: TextOverflow.fade,
                  softWrap: false,
                ),
                subtitle: _buildSubtitle(itemType, context),
                onTap: () {
                  logger.info(itemType);
                  switch (itemType) {
                    case ItemTypeEnum.song:
                      socketService.emitPlayItemsList(data, null, false);
                      break;
                    case ItemTypeEnum.artist:
                      navigationService.updateNavigationInfo(null, data['uri'], null, null, null);
                      Navigator.of(context).pushNamed(RouteNameUtility.artist);
                      break;
                    case ItemTypeEnum.album:
                      navigationService.updateNavigationInfo(data['uri'], null, null, null, null);
                      Navigator.of(context).pushNamed(RouteNameUtility.album);
                      break;
                    case ItemTypeEnum.playlist:
                      logger.warning('Search results, unhandled item type : ${itemType.toString()}');
                      break;
                  }
                },
              );
            },
          );
        },
      ),
    );
  }

  Widget _buildAlbumArt(ItemTypeEnum itemType, dynamic data) {
    switch (itemType) {
      case ItemTypeEnum.artist:
        return WidgetUtility.buildAlbumArtCircle(data['albumart'], _connectedUser.volumioHost);
      case ItemTypeEnum.album:
        return Padding(
          padding: const EdgeInsets.all(5.0),
          child: WidgetUtility.buildAlbumArt(data['albumart'], _connectedUser.volumioHost),
        );
      case ItemTypeEnum.song:
        return Padding(
          padding: const EdgeInsets.all(5.0),
          child: WidgetUtility.buildAlbumArt(data['albumart'], _connectedUser.volumioHost),
        );
      default:
        return Padding(
          padding: const EdgeInsets.all(5.0),
          child: WidgetUtility.buildAlbumArt(data['albumart'], _connectedUser.volumioHost),
        );
    }
  }

  Widget _buildSubtitle(ItemTypeEnum itemType, BuildContext context) {
    String labelKey = '';
    switch (itemType) {
      case ItemTypeEnum.artist:
        labelKey = 'artist';
        break;
      case ItemTypeEnum.album:
        labelKey = 'album';
        break;
      case ItemTypeEnum.song:
        labelKey = 'song';
        break;
      case ItemTypeEnum.playlist:
        logger.warning('Search results, unhandled item type : ${itemType.toString()}');
        break;
    }

    String subtitle = labelKey.isNotEmpty ? AppLocalizations.of(context).translate(labelKey) : '';

    return Text(
      subtitle,
      overflow: TextOverflow.fade,
      softWrap: false,
    );
  }

  @override
  Widget build(BuildContext context) {
    logger.info('Search page build (index = 1), currentIndex : ${navigationService.currentPage}');
    if (SEARCH_PAGE != navigationService.currentPage) {
      return Container();
    }

    logger.config('Search page build');
    navigationService.navigatorKey = navigatorKey;
    return Navigator(
      key: navigatorKey,
      initialRoute: RouteNameUtility.search,
      onGenerateRoute: (RouteSettings settings) {
        WidgetBuilder builder;
        switch (settings.name) {
          case RouteNameUtility.search:
            builder = (BuildContext context) => _buildSearchPage(context);
            break;
          case RouteNameUtility.album:
            builder = (BuildContext context) => AlbumWidget();
            break;
          case RouteNameUtility.artist:
            builder = (BuildContext context) => ArtistWidget();
            break;
          default:
            throw Exception('Invalid route: ${settings.name}');
        }
        return MaterialPageRoute(builder: builder, settings: settings);
      },
    );
  }
}
