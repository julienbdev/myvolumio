import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:logging/logging.dart';
import 'package:myvolumio/app_localizations.dart';
import 'package:myvolumio/models/last_played_item.dart';
import 'package:myvolumio/models/volumio_user.dart';
import 'package:myvolumio/screens/config_screen.dart';
import 'package:myvolumio/services/navigation_service.dart';
import 'package:myvolumio/services/socket_service.dart';
import 'package:myvolumio/services/stats_service.dart';
import 'package:myvolumio/services/user_service.dart';
import 'package:myvolumio/utilities/item_type_num.dart';
import 'package:myvolumio/utilities/route_name_utility.dart';
import 'package:myvolumio/utilities/widget_utility.dart';
import 'package:myvolumio/widgets/album_widget.dart';
import 'package:myvolumio/widgets/artist_widget.dart';
import 'package:myvolumio/widgets/playlist_content_widget.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final StatsService statsService = GetIt.instance.get<StatsService>();
  final SocketService socketService = GetIt.instance.get<SocketService>();
  final NavigationService navigationService = GetIt.instance.get<NavigationService>();
  VolumioUser _connectedUser;
  final GlobalKey<NavigatorState> navigatorKey = GlobalKey();
  static final logger = Logger('HomePage');
  double _horizontalPadding;
  double _verticalSpacingItem = 20;
  double _bottomTitleSpacing = 16;

  Widget _buildLastPlayedItemsAlbumArt(LastPlayedItem item, String volumioHost) {
    switch (item.type) {
      case ItemTypeEnum.song:
        logger.warning('LastPlayedItem album art, unhandled item type : ${item.type.toString()}');
        return Container(); // return empty container to avoid breaking page display
        break;
      case ItemTypeEnum.artist:
        return WidgetUtility.buildAlbumArtCircle(item.albumArt, volumioHost);
        break;
      case ItemTypeEnum.album:
        return WidgetUtility.buildAlbumArt(item.albumArt, volumioHost);
        break;
      case ItemTypeEnum.playlist:
        return WidgetUtility.buildPlaylistAlbumArt(item.albumArts, volumioHost);
        break;
    }

    return Container(); // return empty container to avoid breaking page display
  }

  Widget _buildAlbumArtTiles(LastPlayedItem item) {
    if (item.type == ItemTypeEnum.playlist) {
      return WidgetUtility.buildPlaylistAlbumArt(item.albumArts, _connectedUser.volumioHost);
    } else {
      return WidgetUtility.buildAlbumArt(item.albumArt, _connectedUser.volumioHost);
    }
  }

  Widget _buildWelcomeTiles(BuildContext context) {
    double tilesHeight = (MediaQuery.of(context).size.height * 25) / 100;
    double tilePadding = 10;
    double tileHeight = (tilesHeight - (tilePadding * 2)) / 3;
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: _horizontalPadding),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: EdgeInsets.symmetric(vertical: _bottomTitleSpacing),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  AppLocalizations.of(context).translate('welcome'),
                  style: TextStyle(
                    fontSize: Theme.of(context).textTheme.headline5.fontSize,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                IconButton(
                  icon: Icon(Icons.settings),
                  padding: EdgeInsets.zero,
                  alignment: Alignment.centerRight,
                  onPressed: () {
                    Navigator.of(context).pushNamed(RouteNameUtility.settings).then((value) => setState(() {}));
                  },
                )
              ],
            ),
          ),
          Container(
            height: tilesHeight,
            child: FutureBuilder<List<LastPlayedItem>>(
              initialData: statsService.cachedWelcomeTiles,
              future: statsService.getWelcomeTiles(),
              builder: (context, snapshot) {
                List<LastPlayedItem> tiles = [];

                if (snapshot.hasData) {
                  tiles = snapshot.data;
                  logger.info('tiles length : ${tiles.length}');
                }

                return GridView.count(
                  childAspectRatio: 3,
                  crossAxisSpacing: tilePadding,
                  mainAxisSpacing: tilePadding,
                  physics: NeverScrollableScrollPhysics(),
                  crossAxisCount: 2,
                  children: List.generate(
                    tiles.length,
                    (index) {
                      return InkWell(
                        onTap: () {
                          switch (tiles[index].type) {
                            case ItemTypeEnum.song:
                              logger.warning('Home tiles unhandled type : ${tiles[index].type}');
                              break;
                            case ItemTypeEnum.artist:
                              navigationService.updateNavigationInfo(null, tiles[index].uri, tiles[index].albumArt, null, tiles[index].title);
                              Navigator.of(context).pushNamed(RouteNameUtility.artist);
                              break;
                            case ItemTypeEnum.album:
                              navigationService.updateNavigationInfo(tiles[index].uri, null, null, tiles[index].title, tiles[index].subTitle);
                              Navigator.of(context).pushNamed(RouteNameUtility.album);
                              break;
                            case ItemTypeEnum.playlist:
                              navigationService.updateNavigationInfo(null, null, null, null, null);
                              navigationService.playlistName = tiles[index].title;
                              Navigator.of(context).pushNamed(RouteNameUtility.playlist);
                              break;
                          }
                        },
                        child: Container(
                          height: tileHeight,
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(4),
                            child: Container(
                              decoration: BoxDecoration(
                                color: Colors.grey[850],
                              ),
                              child: Row(
                                children: [
                                  Container(
                                    height: tileHeight,
                                    width: tileHeight,
                                    child: _buildAlbumArtTiles(tiles[index]),
                                  ),
                                  SizedBox(width: 10),
                                  Expanded(
                                    child: AutoSizeText(
                                      tiles[index].title,
                                      style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: Theme.of(context).textTheme.bodyText2.fontSize,
                                      ),
                                      minFontSize: Theme.of(context).textTheme.bodyText2.fontSize,
                                      maxLines: 2,
                                      wrapWords: false,
                                      overflowReplacement: Text(
                                        // in case wrapWords is not working
                                        tiles[index].title,
                                        style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: Theme.of(context).textTheme.bodyText2.fontSize,
                                        ),
                                        overflow: TextOverflow.ellipsis,
                                      ),
                                    ),
                                  ),
                                  SizedBox(width: 10),
                                ],
                              ),
                            ),
                          ),
                        ),
                      );
                    },
                  ),
                );
              },
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildFavoriteAlbums(BuildContext context, double listHeight) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: EdgeInsets.fromLTRB(_horizontalPadding, _verticalSpacingItem, _horizontalPadding, _bottomTitleSpacing),
          child: Text(
            AppLocalizations.of(context).translate('favoriteAlbums'),
            style: TextStyle(
              fontSize: Theme.of(context).textTheme.headline5.fontSize,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
        Container(
          height: listHeight + Theme.of(context).textTheme.bodyText2.fontSize + 2,
          child: FutureBuilder<List<LastPlayedItem>>(
            initialData: statsService.cachedFavoriteAlbums,
            future: statsService.getFavoriteAlbums(),
            builder: (context, snapshot) {
              List<LastPlayedItem> favoriteAlbums = [];
              if (snapshot.hasData) {
                logger.info('home page favorite albums stream has data');
                favoriteAlbums = snapshot.data;
              }

              return ListView.separated(
                padding: EdgeInsets.symmetric(horizontal: _horizontalPadding),
                scrollDirection: Axis.horizontal,
                itemCount: favoriteAlbums.length,
                itemBuilder: (context, index) {
                  return InkWell(
                    onTap: () {
                      navigationService.updateNavigationInfo(
                        favoriteAlbums[index].uri,
                        null,
                        favoriteAlbums[index].albumArt,
                        favoriteAlbums[index].title,
                        favoriteAlbums[index].subTitle,
                      );
                      Navigator.of(context).pushNamed(RouteNameUtility.album);
                    },
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        // Album art
                        Container(
                          height: (listHeight * 80) / 100,
                          width: (listHeight * 80) / 100,
                          child: _buildLastPlayedItemsAlbumArt(favoriteAlbums[index], _connectedUser.volumioHost),
                        ),
                        // Title / subTitle
                        Expanded(
                          child: Container(
                            width: (listHeight * 80) / 100,
                            padding: const EdgeInsets.only(top: 8.0),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  favoriteAlbums[index].title,
                                  overflow: TextOverflow.fade,
                                  softWrap: false,
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                                Text(
                                  favoriteAlbums[index].subTitle,
                                  overflow: TextOverflow.fade,
                                  softWrap: false,
                                  style: TextStyle(
                                    color: Colors.grey,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        )
                      ],
                    ),
                  );
                },
                separatorBuilder: (BuildContext context, int index) {
                  return SizedBox(
                    width: 20,
                  );
                },
              );
            },
          ),
        ),
      ],
    );
  }

  Widget _buildFavoriteArtists(BuildContext context, double listHeight) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: EdgeInsets.fromLTRB(_horizontalPadding, _verticalSpacingItem, _horizontalPadding, _bottomTitleSpacing),
          child: Text(
            AppLocalizations.of(context).translate('favoriteArtists'),
            style: TextStyle(
              fontSize: Theme.of(context).textTheme.headline5.fontSize,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
        Container(
          height: listHeight + Theme.of(context).textTheme.bodyText2.fontSize + 2,
          child: FutureBuilder<List<LastPlayedItem>>(
            initialData: statsService.cachedFavoriteArtists,
            future: statsService.getFavoriteArtists(),
            builder: (context, snapshot) {
              List<LastPlayedItem> favoriteArtists = [];
              if (snapshot.hasData) {
                logger.info('home page favorite artists stream has data');
                favoriteArtists = snapshot.data;
              }

              return ListView.separated(
                padding: EdgeInsets.symmetric(horizontal: _horizontalPadding),
                scrollDirection: Axis.horizontal,
                itemCount: favoriteArtists.length,
                itemBuilder: (context, index) {
                  return InkWell(
                    onTap: () {
                      navigationService.updateNavigationInfo(null, favoriteArtists[index].uri, favoriteArtists[index].albumArt, null, favoriteArtists[index].title);
                      Navigator.of(context).pushNamed(RouteNameUtility.artist);
                    },
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        // Album art
                        Container(
                          height: (listHeight * 80) / 100,
                          width: (listHeight * 80) / 100,
                          child: _buildLastPlayedItemsAlbumArt(favoriteArtists[index], _connectedUser.volumioHost),
                        ),
                        // Title
                        Expanded(
                          child: Container(
                            width: (listHeight * 80) / 100,
                            padding: const EdgeInsets.only(top: 8.0),
                            child: Text(
                              favoriteArtists[index].title,
                              overflow: TextOverflow.fade,
                              textAlign: TextAlign.center,
                              softWrap: false,
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  );
                },
                separatorBuilder: (BuildContext context, int index) {
                  return SizedBox(
                    width: 20,
                  );
                },
              );
            },
          ),
        ),
      ],
    );
  }

  Widget _buildLastPlayedItems(BuildContext context, double listHeight) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: EdgeInsets.fromLTRB(_horizontalPadding, _verticalSpacingItem, _horizontalPadding, _bottomTitleSpacing),
          child: Text(
            AppLocalizations.of(context).translate('recentlyPlayed'),
            style: TextStyle(
              fontSize: Theme.of(context).textTheme.headline5.fontSize,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
        Container(
          height: listHeight + Theme.of(context).textTheme.bodyText2.fontSize + 2,
          child: FutureBuilder<List<LastPlayedItem>>(
            initialData: statsService.cachedLastPlayedItems,
            future: statsService.getLastPlayedItems(),
            builder: (context, snapshot) {
              List<LastPlayedItem> lastPlayedItems = [];
              if (snapshot.hasData) {
                lastPlayedItems = snapshot.data;
              }

              return ListView.separated(
                padding: EdgeInsets.symmetric(horizontal: _horizontalPadding),
                scrollDirection: Axis.horizontal,
                itemCount: lastPlayedItems.length,
                itemBuilder: (context, index) {
                  return InkWell(
                    onTap: () {
                      switch (lastPlayedItems[index].type) {
                        case ItemTypeEnum.song:
                          logger.warning('Home tiles unhandled type : ${lastPlayedItems[index].type}');
                          break;
                        case ItemTypeEnum.artist:
                          navigationService.updateNavigationInfo(null, lastPlayedItems[index].uri, lastPlayedItems[index].albumArt, lastPlayedItems[index].title, null);
                          Navigator.of(context).pushNamed(RouteNameUtility.artist);
                          break;
                        case ItemTypeEnum.album:
                          navigationService.updateNavigationInfo(lastPlayedItems[index].uri, null, null, lastPlayedItems[index].title, lastPlayedItems[index].subTitle);
                          Navigator.of(context).pushNamed(RouteNameUtility.album);
                          break;
                        case ItemTypeEnum.playlist:
                          navigationService.updateNavigationInfo(null, null, null, null, null);
                          navigationService.playlistName = lastPlayedItems[index].title;
                          Navigator.of(context).pushNamed(RouteNameUtility.playlist);
                          break;
                      }
                    },
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        // Album art
                        Container(
                          height: (listHeight * 80) / 100,
                          width: (listHeight * 80) / 100,
                          child: _buildLastPlayedItemsAlbumArt(lastPlayedItems[index], _connectedUser.volumioHost),
                        ),
                        // Title
                        Expanded(
                          child: Container(
                            width: (listHeight * 80) / 100,
                            padding: const EdgeInsets.only(top: 8.0),
                            child: Text(
                              lastPlayedItems[index].title,
                              textAlign: lastPlayedItems[index].type == ItemTypeEnum.artist ? TextAlign.center : TextAlign.start,
                              maxLines: 2,
                              overflow: TextOverflow.fade,
                              softWrap: true,
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  );
                },
                separatorBuilder: (BuildContext context, int index) {
                  return SizedBox(
                    width: 20,
                  );
                },
              );
            },
          ),
        ),
      ],
    );
  }

  Widget _buildHomePage(BuildContext context, double listHeight) {
    _horizontalPadding = (MediaQuery.of(context).size.width * 5) / 100;

    return Container(
      height: double.infinity,
      width: double.infinity,
      decoration: BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          colors: [Colors.blueGrey.withOpacity(0.5), Colors.black.withOpacity(0.6)],
          stops: [0.0, 0.2],
          tileMode: TileMode.repeated,
        ),
      ),
      child: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            children: [
              _buildWelcomeTiles(context),
              _buildLastPlayedItems(context, listHeight),
              _buildFavoriteAlbums(context, listHeight),
              _buildFavoriteArtists(context, listHeight),
            ],
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    logger.info('Home page build (index = 0), currentIndex : ${navigationService.currentPage}');
    if (HOME_PAGE != navigationService.currentPage) {
      return Container();
    }
    _connectedUser = GetIt.instance.get<UserService>().getUser;
    double listHeight = (MediaQuery.of(context).size.height * 20) / 100;

    logger.config('Home page build');

    navigationService.navigatorKey = navigatorKey;
    return Navigator(
      key: navigatorKey,
      initialRoute: RouteNameUtility.home,
      onGenerateRoute: (RouteSettings settings) {
        WidgetBuilder builder;
        logger.info('route name : ${settings.name}');
        switch (settings.name) {
          case RouteNameUtility.home:
            builder = (BuildContext context) => _buildHomePage(context, listHeight);
            break;
          case RouteNameUtility.album:
            builder = (BuildContext context) => AlbumWidget();
            break;
          case RouteNameUtility.artist:
            builder = (BuildContext context) => ArtistWidget();
            break;
          case RouteNameUtility.playlist:
            builder = (BuildContext context) => PlaylistContentWidget();
            break;
          case RouteNameUtility.settings:
            builder = (BuildContext context) => ConfigScreen();
            break;
          default:
            throw Exception('Invalid route: ${settings.name}');
        }
        return MaterialPageRoute(builder: builder, settings: settings);
      },
    );
  }
}
