import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:logging/logging.dart';
import 'package:myvolumio/app_localizations.dart';
import 'package:myvolumio/services/navigation_service.dart';
import 'package:myvolumio/services/socket_service.dart';
import 'package:myvolumio/utilities/route_name_utility.dart';
import 'package:myvolumio/widgets/album_widget.dart';
import 'package:myvolumio/widgets/albums_widget.dart';
import 'package:myvolumio/widgets/artist_widget.dart';
import 'package:myvolumio/widgets/artists_widget.dart';
import 'package:myvolumio/widgets/playlist_content_widget.dart';
import 'package:myvolumio/widgets/playlists_widget.dart';

class LibraryPage extends StatefulWidget {
  @override
  _LibraryPageState createState() => _LibraryPageState();
}

class _LibraryPageState extends State<LibraryPage> with SingleTickerProviderStateMixin {
  final NavigationService navigationService = GetIt.instance.get<NavigationService>();
  final SocketService socketService = GetIt.instance.get<SocketService>();
  TabController _tabController;
  final GlobalKey<NavigatorState> navigatorKey = GlobalKey();
  static final logger = Logger('LibraryPage');

  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: 3, vsync: this);
    _tabController.addListener(_handleTabSelection);
  }

  void _handleTabSelection() {
    if (!_tabController.indexIsChanging) {
      if (_tabController.index == 1) {
        socketService.emitBrowseLibraryArtists();
      }
      if (_tabController.index == 2) {
        socketService.emitBrowseLibraryAlbums();
      }
    }
  }

  Widget _buildTabController(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        bottom: TabBar(
          controller: _tabController,
          tabs: [
            Tab(
              text: AppLocalizations.of(context).translate('playlists'),
            ),
            Tab(
              text: AppLocalizations.of(context).translate('artists'),
            ),
            Tab(
              text: AppLocalizations.of(context).translate('albums'),
            ),
          ],
        ),
        title: Text(
          AppLocalizations.of(context).translate('music'),
          style: TextStyle(
            fontSize: Theme.of(context).textTheme.headline5.fontSize,
          ),
        ),
      ),
      body: TabBarView(
        controller: _tabController,
        children: [
          PlaylistsWidget(),
          ArtistsWidget(),
          AlbumsWidget(),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    logger.info('Library page build (index = 2), currentIndex : ${navigationService.currentPage}');
    if (LIBRARY_PAGE != navigationService.currentPage) {
      return Container();
    }

    logger.config('Library page build');
    navigationService.navigatorKey = navigatorKey;
    return Navigator(
      key: navigatorKey,
      initialRoute: RouteNameUtility.library,
      onGenerateRoute: (RouteSettings settings) {
        WidgetBuilder builder;
        switch (settings.name) {
          case RouteNameUtility.library:
            builder = (BuildContext context) => _buildTabController(context);
            break;
          case RouteNameUtility.album:
            builder = (BuildContext context) => AlbumWidget();
            break;
          case RouteNameUtility.artist:
            builder = (BuildContext context) => ArtistWidget();
            break;
          case RouteNameUtility.playlist:
            builder = (BuildContext context) => PlaylistContentWidget();
            break;
          default:
            throw Exception('Invalid route: ${settings.name}');
        }
        return MaterialPageRoute(builder: builder, settings: settings);
      },
    );
  }
}
