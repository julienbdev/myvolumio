class SongItem {
  String service;
  String uri;
  String title;
  String artist;
  String album;
  String albumart;

  SongItem({this.service, this.uri, this.title, this.artist, this.album, this.albumart});
}
