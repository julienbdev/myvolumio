import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:myvolumio/utilities/item_type_num.dart';

class LastPlayedItem {
  String title;
  String subTitle;
  ItemTypeEnum type;
  List<String> albumArts; // only for type = playlist
  String albumArt;
  String uri;
  Timestamp created;
  int count;

  LastPlayedItem();

  LastPlayedItem.fromJson(Map<String, dynamic> json) {
    title = json['title'];
    subTitle = json['subTitle'];
    albumArt = json['albumArt'];
    uri = json['uri'];
    type = ItemTypeEnum.values.firstWhere((e) => e.toString() == json['type']);
    albumArts = json['albumArts'] != null ? List<String>.from(json['albumArts']) : [];
    created = json['created'];
    count = json['count'];
  }

  Map<String, dynamic> toJson() => {
        'title': title,
        'subTitle': subTitle,
        'albumArt': albumArt,
        'uri': uri,
        'type': type.toString(),
        'albumArts': albumArts,
        'created': created,
        'count': count,
      };

  @override
  String toString() {
    return '{ ${this.title}, ${this.subTitle}, ${this.type}, ${this.albumArts},  ${this.albumArt}, ${this.uri}, ${this.created}}';
  }
}
