class Toast {
  String title;
  String message;
  String type; // success

  Toast(this.type, this.title, this.message);
}