class VolumioUser {
  String id;
  String name;
  String email;
  String volumioHost;
  int volumioPort;

  bool isConfigured() {
    return volumioHost != null && volumioHost.isNotEmpty && volumioPort != null;
  }

  String getSocketUrl() {
    String url = '';
    if(!volumioHost.startsWith('http')) {
      url = 'http://';
    }
    url+=volumioHost;
    url+=':';
    url+=volumioPort.toString();
    
    return url;
  }
}
