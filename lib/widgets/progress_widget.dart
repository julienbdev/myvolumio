import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:myvolumio/services/player_service.dart';

class ProgressWidget extends StatefulWidget {
  @override
  _ProgressWidgetState createState() => _ProgressWidgetState();
}

class _ProgressWidgetState extends State<ProgressWidget> {
  double _progress = 0;

  final locator = GetIt.instance;
  PlayerService playerService;
  int position = 0;
  int musicLength = 0;

  @override
  void initState() {
    super.initState();
    playerService = locator.get<PlayerService>();
    playerService.lengthStream.listen((event) {
      if (!mounted) return;

      setState(() {
        musicLength = event;
        if (musicLength != 0) {
          _progress = (position * 100 / musicLength) / 100;
        }
      });
    });
    playerService.positionStream.listen((event) {
      if (!mounted) return;

      setState(() {
        position = event;
        if (musicLength != 0) {
          _progress = (position * 100 / musicLength) / 100;
        }
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return LinearProgressIndicator(
      minHeight: 1,
      value: _progress,
    );
  }
}
