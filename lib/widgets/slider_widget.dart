import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:myvolumio/services/player_service.dart';
import 'package:myvolumio/services/socket_service.dart';
import 'package:myvolumio/utilities/style_utility.dart';

class SliderWidget extends StatefulWidget {
  @override
  _SliderWidgetState createState() => _SliderWidgetState();
}

class _SliderWidgetState extends State<SliderWidget> {
  final locator = GetIt.instance;
  PlayerService playerService;
  SocketService socketService;
  int position = 0;
  int musicLength = 0;

  @override
  void initState() {
    super.initState();
    socketService = locator.get<SocketService>();
    playerService = locator.get<PlayerService>();
    position = playerService.position;
    musicLength = playerService.length;
    
    playerService.lengthStream.listen((event) {
      if (!mounted) return;

      setState(() {
        musicLength = event;
      });
    });
    playerService.positionStream.listen((event) {
      if (!mounted) return;

      setState(() {
        position = event;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Stack(children: [
      SliderTheme(
        data: SliderThemeData(
          trackShape: CustomTrackShape(),
          activeTrackColor: Colors.tealAccent,
          inactiveTrackColor: Colors.grey,
          trackHeight: 3,
          thumbColor: Colors.tealAccent,
          thumbShape: RoundSliderThumbShape(enabledThumbRadius: 7.0),
        ),
        child: Slider(
          value: position.toDouble(),
          max: musicLength.toDouble(),
          onChanged: (value) {
            socketService.emitSeek(value.round());
          },
        ),
      ),
      Positioned(
        bottom: 0.0,
        child: Text(StyleUtility.formatDuration(Duration(seconds: position))),
      ),
      Positioned(
        bottom: 0.0,
        right: 0.0,
        child: Text(StyleUtility.formatDuration(Duration(seconds: musicLength))),
      ),
    ]);
  }
}

// make slider with full width
class CustomTrackShape extends RoundedRectSliderTrackShape {
  Rect getPreferredRect({
    @required RenderBox parentBox,
    Offset offset = Offset.zero,
    @required SliderThemeData sliderTheme,
    bool isEnabled = false,
    bool isDiscrete = false,
  }) {
    final double trackHeight = sliderTheme.trackHeight;
    final double trackLeft = offset.dx;
    final double trackTop = offset.dy + (parentBox.size.height - trackHeight) / 2;
    final double trackWidth = parentBox.size.width;
    return Rect.fromLTWH(trackLeft, trackTop, trackWidth, trackHeight);
  }
}
