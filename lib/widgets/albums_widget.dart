import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:logging/logging.dart';
import 'package:myvolumio/models/volumio_user.dart';
import 'package:myvolumio/services/navigation_service.dart';
import 'package:myvolumio/services/socket_service.dart';
import 'package:myvolumio/services/user_service.dart';
import 'package:myvolumio/utilities/route_name_utility.dart';
import 'package:myvolumio/utilities/widget_utility.dart';

class AlbumsWidget extends StatefulWidget {
  @override
  _AlbumsWidgetState createState() => _AlbumsWidgetState();
}

class _AlbumsWidgetState extends State<AlbumsWidget> {
  final locator = GetIt.instance;
  SocketService socketService;
  NavigationService navigationService;
  VolumioUser _connectedUser;
  static final logger = Logger('ArtistWidget');

  @override
  initState() {
    super.initState();
    socketService = locator.get<SocketService>();
    navigationService = locator.get<NavigationService>();
  }

  @override
  Widget build(BuildContext context) {
    _connectedUser = locator.get<UserService>().getUser;

    return Container(
      height: double.infinity,
      width: double.infinity,
      color: Colors.black.withOpacity(0.6),
      child: StreamBuilder<dynamic>(
        stream: socketService.getBrowseLibraryStream(),
        initialData: socketService.cachedAlbums,
        builder: (context, snapshot) {
          List<dynamic> albums = [];
          logger.info('build albums');
          if (snapshot.hasData) {
            albums = List<dynamic>.from(snapshot.data['navigation']['lists'][0]['items']);
            if (!(albums.first['uri'] as String).startsWith('albums')) {
              logger.warning('NOT ALBUMS !!!!');
              albums = socketService.cachedAlbums != null ? socketService.cachedAlbums['navigation']['lists'][0]['items'] : [];
            } else {
              socketService.cachedAlbums = snapshot.data;
            }
          }
          return ListView.builder(
            itemCount: albums.length,
            itemBuilder: (context, index) {
              return ListTile(
                leading: Padding(
                  padding: const EdgeInsets.all(5.0),
                  child: WidgetUtility.buildAlbumArt(albums[index]['albumart'], _connectedUser.volumioHost),
                ),
                title: Text(
                  '${albums[index]['title']}',
                  overflow: TextOverflow.fade,
                  softWrap: false,
                ),
                onTap: () {
                  navigationService.updateNavigationInfo(albums[index]['uri'], null, null, null, null);
                  Navigator.of(context).pushNamed(RouteNameUtility.album);
                },
              );
            },
          );
        },
      ),
    );
  }
}
