import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:logging/logging.dart';
import 'package:myvolumio/models/volumio_user.dart';
import 'package:myvolumio/services/navigation_service.dart';
import 'package:myvolumio/services/socket_service.dart';
import 'package:myvolumio/services/user_service.dart';
import 'package:myvolumio/utilities/route_name_utility.dart';
import 'package:myvolumio/utilities/widget_utility.dart';

class ArtistsWidget extends StatefulWidget {
  @override
  _ArtistsWidgetState createState() => _ArtistsWidgetState();
}

class _ArtistsWidgetState extends State<ArtistsWidget> {
  final locator = GetIt.instance;
  SocketService socketService;
  NavigationService navigationService;
  VolumioUser _connectedUser;
  static final logger = Logger('ArtistsWidget');

  @override
  initState() {
    super.initState();
    socketService = locator.get<SocketService>();
    navigationService = locator.get<NavigationService>();
  }

  @override
  Widget build(BuildContext context) {
    _connectedUser = locator.get<UserService>().getUser;

    return Container(
      height: double.infinity,
      width: double.infinity,
      color: Colors.black.withOpacity(0.6),
      child: StreamBuilder<dynamic>(
        stream: socketService.getBrowseLibraryStream(),
        initialData: socketService.cachedArtists,
        builder: (context, snapshot) {
          List<dynamic> artists = [];
          logger.info('build artists');
          if (snapshot.hasData) {
            artists = List<dynamic>.from(snapshot.data['navigation']['lists'][0]['items']);
            if (!(artists.first['uri'] as String).startsWith('artists')) {
              logger.severe('NOT ARTISTS !!!!');
              artists = socketService.cachedArtists != null ? socketService.cachedArtists['navigation']['lists'][0]['items'] : [];
            } else {
              socketService.cachedArtists = snapshot.data;
            }
          }
          return ListView.builder(
            itemCount: artists.length,
            itemBuilder: (context, index) {
              return ListTile(
                leading: WidgetUtility.buildAlbumArtCircle(artists[index]['albumart'], _connectedUser.volumioHost),
                title: Text(
                  '${artists[index]['title']}',
                  overflow: TextOverflow.fade,
                  softWrap: false,
                ),
                onTap: () {
                  navigationService.artistUri = artists[index]['uri'];
                  Navigator.of(context).pushNamed(RouteNameUtility.artist);
                },
              );
            },
          );
        },
      ),
    );
  }
}
