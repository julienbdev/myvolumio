import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:myvolumio/services/navigation_service.dart';
import 'package:myvolumio/services/socket_service.dart';
import 'package:myvolumio/utilities/route_name_utility.dart';

class PlaylistsWidget extends StatefulWidget {
  @override
  _PlaylistsWidgetState createState() => _PlaylistsWidgetState();
}

class _PlaylistsWidgetState extends State<PlaylistsWidget> {
  final locator = GetIt.instance;
  SocketService socketService;
  NavigationService navigationService;

  @override
  initState() {
    super.initState();
    socketService = locator.get<SocketService>();
    socketService.emitPlaylists();
    navigationService = locator.get<NavigationService>();
  }

  Widget _buildPLaylists(BuildContext context) {
    return Container(
      height: double.infinity,
      width: double.infinity,
      color: Colors.black.withOpacity(0.6),
      child: StreamBuilder<dynamic>(
        stream: socketService.getPlaylistsStream(),
        builder: (context, snapshot) {
          List<String> playlists = [];
          if (snapshot.hasData) {
            playlists = List<String>.from(snapshot.data);
          }
          return ListView.builder(
            itemCount: playlists.length,
            itemBuilder: (context, index) {
              return ListTile(
                leading: Icon(Icons.format_list_bulleted),
                title: Text('${playlists[index]}'),
                onTap: () {
                  navigationService.playlistName = playlists[index];
                  Navigator.of(context).pushNamed(RouteNameUtility.playlist);
                },
              );
            },
          );
        },
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return _buildPLaylists(context);
  }
}
