import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:logging/logging.dart';
import 'package:myvolumio/app_localizations.dart';
import 'package:myvolumio/models/last_played_item.dart';
import 'package:myvolumio/models/volumio_goto.dart';
import 'package:myvolumio/models/volumio_user.dart';
import 'package:myvolumio/services/navigation_service.dart';
import 'package:myvolumio/services/socket_service.dart';
import 'package:myvolumio/services/stats_service.dart';
import 'package:myvolumio/services/user_service.dart';
import 'package:myvolumio/utilities/item_type_num.dart';
import 'package:myvolumio/utilities/route_name_utility.dart';
import 'package:myvolumio/utilities/sliver_app_bar_delegate.dart';
import 'package:myvolumio/utilities/widget_utility.dart';
import 'package:myvolumio/widgets/custom_flexible_space_widget.dart';

class ArtistWidget extends StatefulWidget {
  @override
  _ArtistWidgetState createState() => _ArtistWidgetState();
}

class _ArtistWidgetState extends State<ArtistWidget> {
  final locator = GetIt.instance;
  SocketService socketService;
  NavigationService navigationService;
  StatsService statsService;
  VolumioUser _connectedUser;
  static final logger = Logger('ArtistWidget');

  double _buttonVerticalMargin;
  double _buttonFontSize;
  double _buttonPadding = 15;

  @override
  initState() {
    super.initState();
    socketService = locator.get<SocketService>();
    navigationService = locator.get<NavigationService>();
    statsService = locator.get<StatsService>();

    if (navigationService.artistUri != null && navigationService.artistUri.isNotEmpty) {
      socketService.emitBrowseLibrary(navigationService.artistUri);
    } else {
      VolumioGoTo goto = VolumioGoTo();
      goto.value = navigationService.artistGoto;
      goto.album = navigationService.albumGoto;
      goto.artist = navigationService.artistGoto;
      goto.type = 'artist';
      socketService.emitGoTo(goto);
    }
  }

  LastPlayedItem _buildLastPlayedItemArtist(dynamic artistInfo) {
    LastPlayedItem item = LastPlayedItem();

    item.type = ItemTypeEnum.artist;
    item.title = artistInfo['title']; // artist
    item.albumArt = artistInfo['albumart'];
    item.uri = artistInfo['uri'];

    return item;
  }

  Widget _buildPlayRandomButton(BuildContext context, dynamic artistInfo) {
    return Container(
      width: double.infinity,
      child: Padding(
        padding: EdgeInsets.fromLTRB(
          (MediaQuery.of(context).size.width * 10) / 100, // left
          _buttonVerticalMargin, // top
          (MediaQuery.of(context).size.width * 10) / 100, // right
          _buttonVerticalMargin, // bottom
        ),
        child: RaisedButton(
          padding: EdgeInsets.all(_buttonPadding),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(30.0),
          ),
          child: Text(
            AppLocalizations.of(context).translate('randomPlay').toUpperCase(),
            style: TextStyle(fontSize: _buttonFontSize),
          ),
          onPressed: () {
            statsService.addLastPlayedItemArtist(artistInfo['title'], artistInfo['uri'], artistInfo['albumart']);
            statsService.addFavoriteArtist(_buildLastPlayedItemArtist(artistInfo));
            socketService.emitPlayItemsList({'uri': artistInfo['uri']}, null, null, true);
          },
        ),
      ),
    );
  }

  Widget _getTitle(String text, BuildContext context) {
    return Container(
      width: double.infinity,
      padding: const EdgeInsets.all(8.0),
      decoration: BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          colors: [Colors.transparent, Colors.black26],
          stops: [0.0, 0.3],
          tileMode: TileMode.repeated,
        ),
      ),
      child: Text(
        text,
        textAlign: TextAlign.center,
        style: TextStyle(
          color: Colors.white,
          fontSize: Theme.of(context).textTheme.headline3.fontSize,
          fontWeight: FontWeight.w900,
        ),
      ),
    );
  }

  Widget _getImage(String imageUri, String volumioHost) {
    return Container(
      width: double.infinity,
      child: Image.network(
        WidgetUtility.buildAlbumArtUrl(imageUri, volumioHost),
        fit: BoxFit.cover,
      ),
    );
  }

  Widget _buildCollapsedSliver(String title) {
    return Center(
      child: AppBar(
        title: Text(
          title,
          overflow: TextOverflow.fade,
          softWrap: false,
        ),
        centerTitle: true,
      ),
    );
  }

  Widget _buildExpandedSliver(String title, String imageUri, String volumioHost, BuildContext context) {
    return Stack(
      alignment: Alignment.bottomCenter,
      children: [
        _getImage(imageUri, volumioHost),
        _getTitle(
          title,
          context,
        )
      ],
    );
  }

  Widget _buildArtist(BuildContext context) {
    return StreamBuilder(
      stream: socketService.getArtistLibraryStream(),
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          dynamic artistInfo = snapshot.data['navigation']['info'];
          List<dynamic> albums = snapshot.data['navigation']['lists'][0]['items'];

          return Scaffold(
            body: Container(
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [Color(0xFF1F2B2F).withOpacity(0.2), Colors.black.withOpacity(0.6)],
                  stops: [0.0, 0.4],
                  tileMode: TileMode.repeated,
                ),
              ),
              child: CustomScrollView(slivers: <Widget>[
                SliverAppBar(
                  leading: IconButton(
                    icon: Icon(Icons.arrow_back),
                    onPressed: () {
                      logger.info('artist back button pressed');
                      navigationService.updateNavigationInfo(null, null, null, null, null);
                      Navigator.of(context).pop();
                    },
                  ),
                  expandedHeight: (MediaQuery.of(context).size.height * 40) / 100,
                  floating: false,
                  pinned: true,
                  elevation: 4,
                  flexibleSpace: CustomFlexibleSpace(
                    _buildExpandedSliver(artistInfo['title'], artistInfo['albumart'], _connectedUser.volumioHost, context),
                    _buildCollapsedSliver(artistInfo['title']),
                  ),
                ),
                SliverPersistentHeader(
                  pinned: true,
                  delegate: SliverAppBarDelegate(
                    _buildPlayRandomButton(context, artistInfo),
                    (_buttonVerticalMargin * 2) + (_buttonPadding * 2) + _buttonFontSize,
                  ),
                ),
                SliverList(
                  delegate: SliverChildBuilderDelegate(
                    (BuildContext context, int index) {
                      return ListTile(
                        leading: Padding(
                          padding: const EdgeInsets.all(5.0),
                          child: WidgetUtility.buildAlbumArt(albums[index]['albumart'], _connectedUser.volumioHost),
                        ),
                        title: Text(
                          '${albums[index]['title']}',
                          overflow: TextOverflow.fade,
                          softWrap: false,
                        ),
                        onTap: () {
                          navigationService.updateNavigationInfo(albums[index]['uri'], null, artistInfo['albumart'], null, null);
                          Navigator.of(context).pushNamed(RouteNameUtility.album);
                        },
                      );
                    },
                    childCount: albums.length,
                  ),
                ),
              ]),
            ),
          );
        } else {
          return Container(
            height: double.infinity,
            width: double.infinity,
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [Color(0xFF1F2B2F).withOpacity(0.2), Colors.black.withOpacity(0.6)],
                stops: [0.0, 0.4],
                tileMode: TileMode.repeated,
              ),
            ),
          );
        }
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    _connectedUser = locator.get<UserService>().getUser;
    _buttonVerticalMargin = (MediaQuery.of(context).size.height * 2) / 100;
    _buttonFontSize = Theme.of(context).textTheme.bodyText2.fontSize;

    return _buildArtist(context);
  }
}
