import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:logging/logging.dart';
import 'package:myvolumio/models/volumio_user.dart';
import 'package:myvolumio/services/socket_service.dart';
import 'package:myvolumio/services/user_service.dart';
import 'package:myvolumio/utilities/route_name_utility.dart';

class PlayerWidget extends StatefulWidget {
  @override
  _PlayerWidgetState createState() => _PlayerWidgetState();
}

class _PlayerWidgetState extends State<PlayerWidget> {
  final locator = GetIt.instance;
  SocketService socketService;
  VolumioUser _connectedUser;
  String _status = '';
  static final logger = Logger('PlayerWidget');

  @override
  initState() {
    super.initState();
    socketService = locator.get<SocketService>();
    socketService.emitPlayerState();
  }

  Widget _buildPlayButton(AsyncSnapshot<dynamic> snapshot) {
    _status = snapshot.hasData ? snapshot.data['status'] : '';

    if (_status.isNotEmpty) {
      if (_status != 'play') {
        return IconButton(
          icon: Icon(Icons.play_arrow, size: IconTheme.of(context).size * 1.5),
          onPressed: () {
            // update button immediately to avoid network latence with pushState socket stream
            setState(() {
              _status = 'play';
            });
            socketService.emitPlay();
          },
        );
      }
      if (_status == 'play') {
        return IconButton(
          icon: Icon(Icons.pause, size: IconTheme.of(context).size * 1.5),
          onPressed: () {
            // update button immediately to avoid network latence with pushState socket stream
            setState(() {
              _status = 'pause';
            });
            socketService.emitPause();
          },
        );
      }
    }

    return IconButton(icon: Icon(Icons.play_arrow, size: IconTheme.of(context).size * 1.5), onPressed: null);
  }

  Widget _buildAlbumArt(AsyncSnapshot<dynamic> snapshot) {
    if (snapshot.hasData && _connectedUser != null && _connectedUser.volumioHost != null) {
      String albumArtUrl = '';
      if (!_connectedUser.volumioHost.startsWith('http')) {
        albumArtUrl = 'http://';
      }
      albumArtUrl += _connectedUser.volumioHost;
      albumArtUrl += snapshot.data['albumart'];

      return AspectRatio(
        aspectRatio: 1,
        child: Image.network(
          albumArtUrl,
          fit: BoxFit.cover,
        ),
      );
    } else {
      return AspectRatio(
        aspectRatio: 1,
        child: Image.asset(
          'assets/images/albumart.png',
          fit: BoxFit.cover,
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    _connectedUser = locator.get<UserService>().getUser;
    logger.info('player widget build');

    return Container(
      decoration: BoxDecoration(
        border: Border(
          bottom: BorderSide(
            color: Colors.black,
          ),
        ),
      ),
      child: StreamBuilder<dynamic>(
        stream: socketService.getPlayserStateStream(),
        builder: (context, snapshot) {
          return InkWell(
            onTap: () {
              Navigator.of(context).pushNamed(RouteNameUtility.player);
            },
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                _buildAlbumArt(snapshot),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 16),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Text(
                          snapshot.hasData ? snapshot.data['title'] : '',
                          overflow: TextOverflow.fade,
                          softWrap: false,
                          style: Theme.of(context).textTheme.subtitle1.copyWith(
                                fontWeight: FontWeight.bold,
                              ),
                        ),
                        Text(
                          snapshot.hasData ? snapshot.data['artist'] : '',
                          overflow: TextOverflow.fade,
                          softWrap: false,
                          style: Theme.of(context).textTheme.subtitle2.copyWith(
                                color: Colors.grey[300],
                              ),
                        ),
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(right: 12),
                  child: _buildPlayButton(snapshot),
                ),
              ],
            ),
          );
        },
      ),
    );
  }
}
