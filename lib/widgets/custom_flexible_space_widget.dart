import 'dart:math' as math;
import 'package:flutter/material.dart';

class CustomFlexibleSpace extends StatelessWidget {
  final Widget expanded;
  final Widget collapsed;

  CustomFlexibleSpace(this.expanded, this.collapsed);

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, c) {
        final settings = context.dependOnInheritedWidgetOfExactType<FlexibleSpaceBarSettings>();
        final deltaExtent = settings.maxExtent - settings.minExtent;
        final t = (1.0 - (settings.currentExtent - settings.minExtent) / deltaExtent).clamp(0.0, 1.0) as double;
        final fadeStart = math.max(0.0, 1.0 - kToolbarHeight / deltaExtent);
        const fadeEnd = 1.0;
        final opacity = 1.0 - Interval(fadeStart, fadeEnd).transform(t);

        return Container(
          child: Stack(
            children: [
              collapsed,
              Opacity(
                opacity: opacity,
                child: expanded,
              ),
            ],
          ),
        );
      },
    );
  }
}
