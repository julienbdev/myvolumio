import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:logging/logging.dart';
import 'package:myvolumio/app_localizations.dart';
import 'package:myvolumio/models/last_played_item.dart';
import 'package:myvolumio/models/volumio_goto.dart';
import 'package:myvolumio/models/volumio_user.dart';
import 'package:myvolumio/services/navigation_service.dart';
import 'package:myvolumio/services/socket_service.dart';
import 'package:myvolumio/services/stats_service.dart';
import 'package:myvolumio/services/user_service.dart';
import 'package:myvolumio/utilities/item_type_num.dart';
import 'package:myvolumio/utilities/widget_utility.dart';

class AlbumWidget extends StatefulWidget {
  @override
  _AlbumWidgetState createState() => _AlbumWidgetState();
}

class _AlbumWidgetState extends State<AlbumWidget> {
  final locator = GetIt.instance;
  SocketService socketService;
  StatsService statsService;
  NavigationService navigationService;
  VolumioUser _connectedUser;
  static final logger = Logger('AlbumWidget');

  double _buttonVerticalMargin;
  double _buttonFontSize;
  double _buttonPadding = 15;
  double _expandedHeight;

  @override
  initState() {
    super.initState();
    socketService = locator.get<SocketService>();
    navigationService = locator.get<NavigationService>();
    statsService = locator.get<StatsService>();
    if (navigationService.albumUri != null && navigationService.albumUri.isNotEmpty) {
      socketService.emitBrowseLibrary(navigationService.albumUri);
    } else {
      VolumioGoTo goto = VolumioGoTo();
      goto.value = navigationService.albumGoto;
      goto.album = navigationService.albumGoto;
      goto.artist = navigationService.artistGoto;
      goto.type = 'album';
      socketService.emitGoTo(goto);
    }
  }

  String _buildSubtitleInfo(dynamic albumInfo, BuildContext context) {
    StringBuffer subtitle = StringBuffer();
    subtitle.write(AppLocalizations.of(context).translate('albumBy'));
    subtitle.write(' ');
    subtitle.write(albumInfo['artist']);

    String year = albumInfo['year'] as String;
    if (year != null && year.isNotEmpty) {
      subtitle.write(' • ');
      subtitle.write(year.length < 5 ? year : year.substring(0, 5));
    }

    return subtitle.toString();
  }

  Widget _buildAlbumInfos(dynamic albumInfo, String volumioHost, BuildContext context) {
    return Container(
      color: Colors.transparent,
      padding: EdgeInsets.only(top: (MediaQuery.of(context).size.height * 5) / 100),
      width: double.infinity,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          SizedBox(
            height: (_expandedHeight / 2) - 16,
            child: WidgetUtility.buildAlbumArt(albumInfo['albumart'], volumioHost),
          ),
          Padding(
            padding: const EdgeInsets.all(10),
            child: Text(
              albumInfo['album'],
              textAlign: TextAlign.center,
              style: TextStyle(
                color: Colors.white,
                fontSize: Theme.of(context).textTheme.headline5.fontSize,
                fontWeight: FontWeight.w900,
              ),
            ),
          ),
          Text(
            _buildSubtitleInfo(albumInfo, context),
            style: TextStyle(
              color: Colors.grey,
            ),
          ),
        ],
      ),
    );
  }

  LastPlayedItem _buildLastPlayedItemAlbum(dynamic albumInfo) {
    LastPlayedItem item = LastPlayedItem();

    item.type = ItemTypeEnum.album;
    item.title = albumInfo['album']; // album
    item.subTitle = albumInfo['artist']; // artist
    item.albumArt = albumInfo['albumart'];
    item.uri = albumInfo['uri'];

    return item;
  }

  LastPlayedItem _buildLastPlayedItemArtist(dynamic albumInfo) {
    LastPlayedItem item = LastPlayedItem();

    item.type = ItemTypeEnum.artist;
    item.title = albumInfo['artist']; // artist
    item.albumArt = navigationService.artistArt;

    return item;
  }

  Widget _buildPlayRandomButton(BuildContext context, dynamic albumInfo) {
    return Container(
      width: double.infinity,
      child: Padding(
        padding: EdgeInsets.fromLTRB(
          (MediaQuery.of(context).size.width * 10) / 100, // left
          _buttonVerticalMargin, // top
          (MediaQuery.of(context).size.width * 10) / 100, // right
          _buttonVerticalMargin, // bottom
        ),
        child: RaisedButton(
          padding: EdgeInsets.all(_buttonPadding),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(30.0),
          ),
          child: Text(
            AppLocalizations.of(context).translate('randomPlay').toUpperCase(),
            style: TextStyle(fontSize: _buttonFontSize),
          ),
          onPressed: () {
            statsService.addLastPlayedItemAlbum(albumInfo['album'], albumInfo['uri'], albumInfo['albumart']);
            statsService.addFavoriteAlbum(_buildLastPlayedItemAlbum(albumInfo));
            statsService.addFavoriteArtist(_buildLastPlayedItemArtist(albumInfo));
            socketService.emitPlayItemsList({'uri': albumInfo['uri']}, null, null, true);
          },
        ),
      ),
    );
  }

  Widget _buildSongs(List<dynamic> songs, dynamic albumInfo) {
    return ListView.builder(
      physics: NeverScrollableScrollPhysics(),
      shrinkWrap: true,
      itemCount: songs.length,
      itemBuilder: (context, index) {
        return ListTile(
          title: Text(
            '${songs[index]['title']}',
            overflow: TextOverflow.fade,
            softWrap: false,
          ),
          subtitle: Text(
            '${songs[index]['artist']}',
            overflow: TextOverflow.fade,
            softWrap: false,
          ),
          onTap: () {
            statsService.addLastPlayedItemAlbum(albumInfo['album'], navigationService.albumUri, albumInfo['albumart']);
            statsService.addFavoriteAlbum(_buildLastPlayedItemAlbum(albumInfo));
            statsService.addFavoriteArtist(_buildLastPlayedItemArtist(albumInfo));
            socketService.emitPlayItemsList(songs[index], songs, index);
          },
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    _connectedUser = locator.get<UserService>().getUser;
    _buttonVerticalMargin = (MediaQuery.of(context).size.height * 2) / 100;
    _buttonFontSize = Theme.of(context).textTheme.bodyText2.fontSize;
    _expandedHeight = (MediaQuery.of(context).size.height * 50) / 100;

    logger.info('album widget build');

    return StreamBuilder(
      stream: socketService.getAlbumLibraryStream(),
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          dynamic albumInfo = snapshot.data['navigation']['info'];
          List<dynamic> songs = snapshot.data['navigation']['lists'][0]['items'];

          return Scaffold(
            appBar: AppBar(
              leading: IconButton(
                icon: Icon(Icons.arrow_back),
                onPressed: () {
                  navigationService.updateNavigationInfo(null, null, null, null, null);
                  Navigator.of(context).pop();
                },
              ),
              title: Text(
                albumInfo['album'],
                overflow: TextOverflow.fade,
                softWrap: false,
              ),
            ),
            body: Container(
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [Color(0xFF1F2B2F).withOpacity(0.2), Colors.black.withOpacity(0.6)],
                  stops: [0.0, 0.4],
                  tileMode: TileMode.repeated,
                ),
              ),
              child: SingleChildScrollView(
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  children: [
                    _buildAlbumInfos(albumInfo, _connectedUser.volumioHost, context),
                    _buildPlayRandomButton(context, albumInfo),
                    _buildSongs(songs, albumInfo),
                  ],
                ),
              ),
            ),
          );
        } else {
          return Container(
            height: double.infinity,
            width: double.infinity,
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [Color(0xFF1F2B2F).withOpacity(0.2), Colors.black.withOpacity(0.6)],
                stops: [0.0, 0.4],
                tileMode: TileMode.repeated,
              ),
            ),
          );
        }
      },
    );
  }
}
