import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:logging/logging.dart';
import 'package:myvolumio/app_localizations.dart';
import 'package:myvolumio/models/last_played_item.dart';
import 'package:myvolumio/models/volumio_user.dart';
import 'package:myvolumio/services/navigation_service.dart';
import 'package:myvolumio/services/socket_service.dart';
import 'package:myvolumio/services/stats_service.dart';
import 'package:myvolumio/services/user_service.dart';
import 'package:myvolumio/utilities/item_type_num.dart';
import 'package:myvolumio/utilities/widget_utility.dart';

class PlaylistContentWidget extends StatefulWidget {
  @override
  _PlaylistContentWidgetState createState() => _PlaylistContentWidgetState();
}

class _PlaylistContentWidgetState extends State<PlaylistContentWidget> {
  final locator = GetIt.instance;
  SocketService socketService;
  NavigationService navigationService;
  StatsService statsService;
  VolumioUser _connectedUser;
  double _buttonVerticalMargin;
  double _buttonFontSize;
  double _buttonPadding = 15;
  double _songListTopPadding;
  double _firstSongTopPadding;
  static final logger = Logger('PlaylistContentWidget');
  List<String> albumArts = [];

  @override
  initState() {
    super.initState();
    socketService = locator.get<SocketService>();
    navigationService = locator.get<NavigationService>();
    statsService = locator.get<StatsService>();
    socketService.emitPlaylistContent(navigationService.playlistName);
  }

  LastPlayedItem _buildLastPlayedItemAlbum(dynamic song) {
    LastPlayedItem item = LastPlayedItem();

    item.type = ItemTypeEnum.album;
    item.title = song['album']; // album
    item.subTitle = song['artist']; // artist
    item.albumArt = song['albumart'];

    return item;
  }

  LastPlayedItem _buildLastPlayedItemArtist(dynamic song) {
    LastPlayedItem item = LastPlayedItem();

    item.type = ItemTypeEnum.artist;
    item.title = song['artist']; // artist

    return item;
  }

  Widget _buildSongList(List<dynamic> songs) {
    logger.info('songs length : ${songs.length}');
    return Padding(
      padding: EdgeInsets.only(top: _songListTopPadding),
      child: ListView.builder(
        itemCount: songs.length,
        itemBuilder: (context, index) {
          return Padding(
            padding: index == 0 ? EdgeInsets.only(top: _firstSongTopPadding) : const EdgeInsets.all(0),
            child: ListTile(
              leading: WidgetUtility.buildAlbumArt(songs[index]['albumart'], _connectedUser.volumioHost),
              title: Text(
                '${songs[index]['title']}',
                overflow: TextOverflow.fade,
                softWrap: false,
              ),
              subtitle: Text(
                '${songs[index]['artist']}',
                overflow: TextOverflow.fade,
                softWrap: false,
              ),
              onTap: () {
                statsService.addLastPlayedItemPlaylist(navigationService.playlistName, albumArts);
                statsService.addFavoritePlaylist(navigationService.playlistName, albumArts);
                statsService.addFavoriteAlbum(_buildLastPlayedItemAlbum(songs[index]));
                statsService.addFavoriteArtist(_buildLastPlayedItemArtist(songs[index]));
                socketService.emitPlayItemsList(songs[index], songs, index);
              },
            ),
          );
        },
      ),
    );
  }

  Widget _buildPlayRandomButton(BuildContext context) {
    return Container(
      width: double.infinity,
      child: Padding(
        padding: EdgeInsets.fromLTRB(
          (MediaQuery.of(context).size.width * 10) / 100, // left
          _buttonVerticalMargin, // top
          (MediaQuery.of(context).size.width * 10) / 100, // right
          _buttonVerticalMargin, // bottom
        ),
        child: RaisedButton(
          padding: EdgeInsets.all(_buttonPadding),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(30.0),
          ),
          child: Text(
            AppLocalizations.of(context).translate('randomPlay').toUpperCase(),
            style: TextStyle(fontSize: _buttonFontSize),
          ),
          onPressed: () {
            statsService.addLastPlayedItemPlaylist(navigationService.playlistName, albumArts);
            statsService.addFavoritePlaylist(navigationService.playlistName, albumArts);
            socketService.emitPlayPlaylist(navigationService.playlistName, true);
          },
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    _connectedUser = locator.get<UserService>().getUser;
    _buttonVerticalMargin = (MediaQuery.of(context).size.height * 2) / 100;
    _buttonFontSize = Theme.of(context).textTheme.bodyText2.fontSize;
    _songListTopPadding = _buttonVerticalMargin + _buttonPadding + (_buttonFontSize / 2);
    _firstSongTopPadding = _buttonPadding + (_buttonFontSize / 2) + 20;

    return Scaffold(
      extendBodyBehindAppBar: true, // for transparent appBar background
      appBar: AppBar(
        title: Text(navigationService.playlistName),
        backgroundColor: Colors.transparent,
        elevation: 0,
        centerTitle: true,
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () {
            navigationService.playlistName = '';
            Navigator.of(context).pop();
          },
        ),
      ),
      body: StreamBuilder<dynamic>(
        stream: socketService.getPlaylistContentStream(),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            albumArts = WidgetUtility.buildPlaylistAlbumArtUrls(snapshot.data['lists'][0]);
          }

          return Container(
            height: double.infinity,
            width: double.infinity,
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [Color(0xFF1F2B2F).withOpacity(0.2), Colors.black.withOpacity(0.6)],
                stops: [0.0, 0.4],
                tileMode: TileMode.repeated,
              ),
            ),
            child: SafeArea(
              child: Stack(
                children: [
                  if (snapshot.hasData) _buildSongList(snapshot.data['lists'][0]),
                  _buildPlayRandomButton(context),
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}
